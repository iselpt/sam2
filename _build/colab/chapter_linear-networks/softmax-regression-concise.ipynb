{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following additional libraries are needed to run this\n",
    "notebook. Note that running on Colab is experimental, please report a Github\n",
    "issue if you have any problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install -U mxnet-cu101mkl==1.6.0.post0  # updating mxnet to at least v1.6\n",
    "!pip install d2l==0.14.3\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 0
   },
   "source": [
    "# Concise Implementation of Softmax Regression\n",
    ":label:`sec_softmax_concise`\n",
    "\n",
    "Just as high-level APIs of deep learning frameworks\n",
    "made it much easier\n",
    "to implement linear regression in :numref:`sec_linear_concise`,\n",
    "we will find it similarly (or possibly more)\n",
    "convenient for implementing classification models. Let us stick with the Fashion-MNIST dataset\n",
    "and keep the batch size at 256 as in :numref:`sec_softmax_scratch`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 1,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "from d2l import mxnet as d2l\n",
    "from mxnet import gluon, init, npx\n",
    "from mxnet.gluon import nn\n",
    "npx.set_np()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 4,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "batch_size = 256\n",
    "train_iter, test_iter = d2l.load_data_fashion_mnist(batch_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 5
   },
   "source": [
    "## Initializing Model Parameters\n",
    "\n",
    "As mentioned in :numref:`sec_softmax`,\n",
    "the output layer of softmax regression\n",
    "is a fully-connected layer.\n",
    "Therefore, to implement our model,\n",
    "we just need to add one fully-connected layer\n",
    "with 10 outputs to our `Sequential`.\n",
    "Again, here, the `Sequential` is not really necessary,\n",
    "but we might as well form the habit since it will be ubiquitous\n",
    "when implementing deep models.\n",
    "Again, we initialize the weights at random\n",
    "with zero mean and standard deviation 0.01.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 6,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "net = nn.Sequential()\n",
    "net.add(nn.Dense(10))\n",
    "net.initialize(init.Normal(sigma=0.01))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 9
   },
   "source": [
    "## Softmax Implementation Revisited\n",
    ":label:`subsec_softmax-implementation-revisited`\n",
    "\n",
    "In the previous example of :numref:`sec_softmax_scratch`,\n",
    "we calculated our model's output\n",
    "and then ran this output through the cross-entropy loss.\n",
    "Mathematically, that is a perfectly reasonable thing to do.\n",
    "However, from a computational perspective,\n",
    "exponentiation can be a source of numerical stability issues.\n",
    "\n",
    "Recall that the softmax function calculates\n",
    "$\\hat y_j = \\frac{\\exp(o_j)}{\\sum_k \\exp(o_k)}$,\n",
    "where $\\hat y_j$ is the $j^\\mathrm{th}$ element of\n",
    "the predicted probability distribution $\\hat{\\mathbf{y}}$\n",
    "and $o_j$ is the $j^\\mathrm{th}$ element of the logits\n",
    "$\\mathbf{o}$.\n",
    "If some of the $o_k$ are very large (i.e., very positive),\n",
    "then $\\exp(o_k)$ might be larger than the largest number\n",
    "we can have for certain data types (i.e., *overflow*).\n",
    "This would make the denominator (and/or numerator) `inf` (infinity)\n",
    "and we wind up encountering either 0, `inf`, or `nan` (not a number) for $\\hat y_j$.\n",
    "In these situations we do not get a well-defined\n",
    "return value for cross entropy.\n",
    "\n",
    "\n",
    "One trick to get around this is to first subtract $\\max(o_k)$\n",
    "from all $o_k$ before proceeding with the softmax calculation.\n",
    "You can verify that this shifting of each $o_k$ by constant factor\n",
    "does not change the return value of softmax.\n",
    "After the subtraction and normalization step,\n",
    "it might be possible that some $o_j$ have large negative values\n",
    "and thus that the corresponding $\\exp(o_j)$ will take values close to zero.\n",
    "These might be rounded to zero due to finite precision (i.e., *underflow*),\n",
    "making $\\hat y_j$ zero and giving us `-inf` for $\\log(\\hat y_j)$.\n",
    "A few steps down the road in backpropagation,\n",
    "we might find ourselves faced with a screenful\n",
    "of the dreaded `nan` results.\n",
    "\n",
    "Fortunately, we are saved by the fact that\n",
    "even though we are computing exponential functions,\n",
    "we ultimately intend to take their log\n",
    "(when calculating the cross-entropy loss).\n",
    "By combining these two operators\n",
    "softmax and cross entropy together,\n",
    "we can escape the numerical stability issues\n",
    "that might otherwise plague us during backpropagation.\n",
    "As shown in the equation below, we avoid calculating $\\exp(o_j)$\n",
    "and can use instead $o_j$ directly due to the canceling in $\\log(\\exp(\\cdot))$.\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\log{(\\hat y_j)} & = \\log\\left( \\frac{\\exp(o_j)}{\\sum_k \\exp(o_k)}\\right) \\\\\n",
    "& = \\log{(\\exp(o_j))}-\\log{\\left( \\sum_k \\exp(o_k) \\right)} \\\\\n",
    "& = o_j -\\log{\\left( \\sum_k \\exp(o_k) \\right)}.\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "We will want to keep the conventional softmax function handy\n",
    "in case we ever want to evaluate the output probabilities by our model.\n",
    "But instead of passing softmax probabilities into our new loss function,\n",
    "we will just pass the logits and compute the softmax and its log\n",
    "all at once inside the cross entropy loss function,\n",
    "which does smart things like the [\"LogSumExp trick\"](https://en.wikipedia.org/wiki/LogSumExp).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 10,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "loss = gluon.loss.SoftmaxCrossEntropyLoss()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 13
   },
   "source": [
    "## Optimization Algorithm\n",
    "\n",
    "Here, we use minibatch stochastic gradient descent\n",
    "with a learning rate of 0.1 as the optimization algorithm.\n",
    "Note that this is the same as we applied in the linear regression example\n",
    "and it illustrates the general applicability of the optimizers.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 14,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.1})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 17
   },
   "source": [
    "## Training\n",
    "\n",
    "Next we call the training function defined in :numref:`sec_softmax_scratch` to train the model.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 18,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "num_epochs = 10\n",
    "d2l.train_ch3(net, train_iter, test_iter, loss, num_epochs, trainer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 19
   },
   "source": [
    "As before, this algorithm converges to a solution\n",
    "that achieves a decent accuracy,\n",
    "albeit this time with fewer lines of code than before.\n",
    "\n",
    "\n",
    "## Summary\n",
    "\n",
    "* Using high-level APIs, we can implement softmax regression much more concisely.\n",
    "* From a computational perspective, implementing softmax regression has intricacies. Note that in many cases, a deep learning framework takes additional precautions beyond these most well-known tricks to ensure numerical stability, saving us from even more pitfalls that we would encounter if we tried to code all of our models from scratch in practice.\n",
    "\n",
    "## Exercises\n",
    "\n",
    "1. Try adjusting the hyperparameters, such as the batch size, number of epochs, and learning rate, to see what the results are.\n",
    "1. Increase the numper of epochs for training. Why might the test accuracy decrease after a while? How could we fix this?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 20,
    "tab": [
     "mxnet"
    ]
   },
   "source": [
    "[Discussions](https://discuss.d2l.ai/t/52)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "name": "python3"
  },
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}