��'i      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�.. _sec_fashion_mnist:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��sec-fashion-mnist�u�tagname�h
�line�K�parent�hhh�source��V/home/mn/sa20/sam2/_build/rst/chapter_linear-networks/image-classification-dataset.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h� The Image Classification Dataset�h]�h	�Text���� The Image Classification Dataset�����}�(hh,h h*hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h%hhh!h"hKubh	�	paragraph���)��}�(hXc  One of the widely used dataset for image classification is the MNIST
dataset :cite:`LeCun.Bottou.Bengio.ea.1998`. While it had a good run
as a benchmark dataset, even simple models by today’s standards achieve
classification accuracy over 95%, making it unsuitable for
distinguishing between stronger models and weaker ones. Today, MNIST
serves as more of sanity checks than as a benchmark. To up the ante just
a bit, we will focus our discussion in the coming sections on the
qualitatively similar, but comparatively complex Fashion-MNIST dataset
:cite:`Xiao.Rasul.Vollgraf.2017`, which was released in 2017.�h]�(h/�MOne of the widely used dataset for image classification is the MNIST
dataset �����}�(h�MOne of the widely used dataset for image classification is the MNIST
dataset �h h<hhh!NhNubh �pending_xref���)��}�(h�LeCun.Bottou.Bengio.ea.1998�h]�h	�inline���)��}�(hhIh]�h/�[LeCun.Bottou.Bengio.ea.1998]�����}�(hhh hMubah}�(h]�h]�h]�h]�h]�uhhKh hGubah}�(h]��id1�ah]��bibtex�ah]�h]�h]��	refdomain��citation��reftype��ref��	reftarget�hI�refwarn���support_smartquotes��uhhEh!h"hKh h<hhubh/X�  . While it had a good run
as a benchmark dataset, even simple models by today’s standards achieve
classification accuracy over 95%, making it unsuitable for
distinguishing between stronger models and weaker ones. Today, MNIST
serves as more of sanity checks than as a benchmark. To up the ante just
a bit, we will focus our discussion in the coming sections on the
qualitatively similar, but comparatively complex Fashion-MNIST dataset
�����}�(hX�  . While it had a good run
as a benchmark dataset, even simple models by today’s standards achieve
classification accuracy over 95%, making it unsuitable for
distinguishing between stronger models and weaker ones. Today, MNIST
serves as more of sanity checks than as a benchmark. To up the ante just
a bit, we will focus our discussion in the coming sections on the
qualitatively similar, but comparatively complex Fashion-MNIST dataset
�h h<hhh!NhNubhF)��}�(h�Xiao.Rasul.Vollgraf.2017�h]�hL)��}�(hhph]�h/�[Xiao.Rasul.Vollgraf.2017]�����}�(hhh hrubah}�(h]�h]�h]�h]�h]�uhhKh hnubah}�(h]��id2�ah]�h^ah]�h]�h]��	refdomain�hc�reftype�he�	reftarget�hp�refwarn���support_smartquotes��uhhEh!h"hKh h<hhubh/�, which was released in 2017.�����}�(h�, which was released in 2017.�h h<hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKh h%hhubh	�literal_block���)��}�(h�i%matplotlib inline
from d2l import mxnet as d2l
from mxnet import gluon
import sys

d2l.use_svg_display()�h]�h/�i%matplotlib inline
from d2l import mxnet as d2l
from mxnet import gluon
import sys

d2l.use_svg_display()�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}��	xml:space��preserve��language��python�uhh�h!h"hKh h%hhubh$)��}�(hhh]�(h))��}�(h�Reading the Dataset�h]�h/�Reading the Dataset�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h�hhh!h"hKubh;)��}�(h�kWe can download and read the Fashion-MNIST dataset into memory via the
build-in functions in the framework.�h]�h/�kWe can download and read the Fashion-MNIST dataset into memory via the
build-in functions in the framework.�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKh h�hhubh�)��}�(h�qmnist_train = gluon.data.vision.FashionMNIST(train=True)
mnist_test = gluon.data.vision.FashionMNIST(train=False)�h]�h/�qmnist_train = gluon.data.vision.FashionMNIST(train=True)
mnist_test = gluon.data.vision.FashionMNIST(train=False)�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK!h h�hhubh;)��}�(hXJ  Fashion-MNIST consists of images from 10 categories, each represented by
6000 images in the training dataset and by 1000 in the test dataset. A
*test dataset* (or *test set*) is used for evaluating model performance
and not for training. Consequently the training set and the test set
contain 60000 and 10000 images, respectively.�h]�(h/��Fashion-MNIST consists of images from 10 categories, each represented by
6000 images in the training dataset and by 1000 in the test dataset. A
�����}�(h��Fashion-MNIST consists of images from 10 categories, each represented by
6000 images in the training dataset and by 1000 in the test dataset. A
�h h�hhh!NhNubh	�emphasis���)��}�(h�*test dataset*�h]�h/�test dataset�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h�ubh/� (or �����}�(h� (or �h h�hhh!NhNubh�)��}�(h�
*test set*�h]�h/�test set�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h�ubh/��) is used for evaluating model performance
and not for training. Consequently the training set and the test set
contain 60000 and 10000 images, respectively.�����}�(h��) is used for evaluating model performance
and not for training. Consequently the training set and the test set
contain 60000 and 10000 images, respectively.�h h�hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK&h h�hhubh�)��}�(h�!len(mnist_train), len(mnist_test)�h]�h/�!len(mnist_train), len(mnist_test)�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK,h h�hhubh;)��}�(hX/  The height and width of each input image are both 28 pixels. Note that
the dataset consists of grayscale images, whose number of channels is 1.
For brevity, throughout this book we store the shape of any image with
height :math:`h` width :math:`w` pixels as :math:`h \times w` or
(:math:`h`, :math:`w`).�h]�(h/��The height and width of each input image are both 28 pixels. Note that
the dataset consists of grayscale images, whose number of channels is 1.
For brevity, throughout this book we store the shape of any image with
height �����}�(h��The height and width of each input image are both 28 pixels. Note that
the dataset consists of grayscale images, whose number of channels is 1.
For brevity, throughout this book we store the shape of any image with
height �h j'  hhh!NhNubh	�math���)��}�(h�	:math:`h`�h]�h/�h�����}�(hhh j2  ubah}�(h]�h]�h]�h]�h]�uhj0  h j'  ubh/� width �����}�(h� width �h j'  hhh!NhNubj1  )��}�(h�	:math:`w`�h]�h/�w�����}�(hhh jE  ubah}�(h]�h]�h]�h]�h]�uhj0  h j'  ubh/� pixels as �����}�(h� pixels as �h j'  hhh!NhNubj1  )��}�(h�:math:`h \times w`�h]�h/�
h \times w�����}�(hhh jX  ubah}�(h]�h]�h]�h]�h]�uhj0  h j'  ubh/� or
(�����}�(h� or
(�h j'  hhh!NhNubj1  )��}�(h�	:math:`h`�h]�h/�h�����}�(hhh jk  ubah}�(h]�h]�h]�h]�h]�uhj0  h j'  ubh/�, �����}�(h�, �h j'  hhh!NhNubj1  )��}�(h�	:math:`w`�h]�h/�w�����}�(hhh j~  ubah}�(h]�h]�h]�h]�h]�uhj0  h j'  ubh/�).�����}�(h�).�h j'  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK0h h�hhubh�)��}�(h�mnist_train[0][0].shape�h]�h/�mnist_train[0][0].shape�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK6h h�hhubh;)��}�(h��The images in Fashion-MNIST are associated with the following
categories: t-shirt, trousers, pullover, dress, coat, sandal, shirt,
sneaker, bag, and ankle boot. The following function converts between
numeric label indices and their names in text.�h]�h/��The images in Fashion-MNIST are associated with the following
categories: t-shirt, trousers, pullover, dress, coat, sandal, shirt,
sneaker, bag, and ankle boot. The following function converts between
numeric label indices and their names in text.�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK:h h�hhubh�)��}�(hX&  def get_fashion_mnist_labels(labels):  #@save
    """Return text labels for the Fashion-MNIST dataset."""
    text_labels = ['t-shirt', 'trouser', 'pullover', 'dress', 'coat',
                   'sandal', 'shirt', 'sneaker', 'bag', 'ankle boot']
    return [text_labels[int(i)] for i in labels]�h]�h/X&  def get_fashion_mnist_labels(labels):  #@save
    """Return text labels for the Fashion-MNIST dataset."""
    text_labels = ['t-shirt', 'trouser', 'pullover', 'dress', 'coat',
                   'sandal', 'shirt', 'sneaker', 'bag', 'ankle boot']
    return [text_labels[int(i)] for i in labels]�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK?h h�hhubh;)��}�(h�9We can now create a function to visualize these examples.�h]�h/�9We can now create a function to visualize these examples.�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKGh h�hhubh�)��}�(hX�  def show_images(imgs, num_rows, num_cols, titles=None, scale=1.5):  #@save
    """Plot a list of images."""
    figsize = (num_cols * scale, num_rows * scale)
    _, axes = d2l.plt.subplots(num_rows, num_cols, figsize=figsize)
    axes = axes.flatten()
    for i, (ax, img) in enumerate(zip(axes, imgs)):
        ax.imshow(d2l.numpy(img))
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        if titles:
            ax.set_title(titles[i])
    return axes�h]�h/X�  def show_images(imgs, num_rows, num_cols, titles=None, scale=1.5):  #@save
    """Plot a list of images."""
    figsize = (num_cols * scale, num_rows * scale)
    _, axes = d2l.plt.subplots(num_rows, num_cols, figsize=figsize)
    axes = axes.flatten()
    for i, (ax, img) in enumerate(zip(axes, imgs)):
        ax.imshow(d2l.numpy(img))
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        if titles:
            ax.set_title(titles[i])
    return axes�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hKIh h�hhubh;)��}�(h�pHere are the images and their corresponding labels (in text) for the
first few examples in the training dataset.�h]�h/�pHere are the images and their corresponding labels (in text) for the
first few examples in the training dataset.�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKXh h�hhubh�)��}�(h�bX, y = mnist_train[:18]
show_images(X.squeeze(axis=-1), 2, 9, titles=get_fashion_mnist_labels(y));�h]�h/�bX, y = mnist_train[:18]
show_images(X.squeeze(axis=-1), 2, 9, titles=get_fashion_mnist_labels(y));�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK[h h�hhubeh}�(h]��reading-the-dataset�ah]�h]��reading the dataset�ah]�h]�uhh#h h%hhh!h"hKubh$)��}�(hhh]�(h))��}�(h�Reading a Minibatch�h]�h/�Reading a Minibatch�����}�(hj  h j  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j  hhh!h"hKaubh;)��}�(hX?  To make our life easier when reading from the training and test sets, we
use the built-in data iterator rather than creating one from scratch.
Recall that at each iteration, a data loader reads a minibatch of data
with size ``batch_size`` each time. We also randomly shuffle the
examples for the training data iterator.�h]�(h/��To make our life easier when reading from the training and test sets, we
use the built-in data iterator rather than creating one from scratch.
Recall that at each iteration, a data loader reads a minibatch of data
with size �����}�(h��To make our life easier when reading from the training and test sets, we
use the built-in data iterator rather than creating one from scratch.
Recall that at each iteration, a data loader reads a minibatch of data
with size �h j"  hhh!NhNubh	�literal���)��}�(h�``batch_size``�h]�h/�
batch_size�����}�(hhh j-  ubah}�(h]�h]�h]�h]�h]�uhj+  h j"  ubh/�Q each time. We also randomly shuffle the
examples for the training data iterator.�����}�(h�Q each time. We also randomly shuffle the
examples for the training data iterator.�h j"  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hKch j  hhubh�)��}�(hXM  batch_size = 256

def get_dataloader_workers():  #@save
    """Use 4 processes to read the data except for Windows."""
    return 0 if sys.platform.startswith('win') else 4

# `ToTensor` converts the image data from uint8 to 32-bit floating point. It
# divides all numbers by 255 so that all pixel values are between 0 and 1
transformer = gluon.data.vision.transforms.ToTensor()
train_iter = gluon.data.DataLoader(mnist_train.transform_first(transformer),
                                   batch_size, shuffle=True,
                                   num_workers=get_dataloader_workers())�h]�h/XM  batch_size = 256

def get_dataloader_workers():  #@save
    """Use 4 processes to read the data except for Windows."""
    return 0 if sys.platform.startswith('win') else 4

# `ToTensor` converts the image data from uint8 to 32-bit floating point. It
# divides all numbers by 255 so that all pixel values are between 0 and 1
transformer = gluon.data.vision.transforms.ToTensor()
train_iter = gluon.data.DataLoader(mnist_train.transform_first(transformer),
                                   batch_size, shuffle=True,
                                   num_workers=get_dataloader_workers())�����}�(hhh jF  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hKih j  hhubh;)��}�(h�;Let us look at the time it takes to read the training data.�h]�h/�;Let us look at the time it takes to read the training data.�����}�(hjZ  h jX  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hKxh j  hhubh�)��}�(h�Rtimer = d2l.Timer()
for X, y in train_iter:
    continue
f'{timer.stop():.2f} sec'�h]�h/�Rtimer = d2l.Timer()
for X, y in train_iter:
    continue
f'{timer.stop():.2f} sec'�����}�(hhh jf  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hKzh j  hhubeh}�(h]��reading-a-minibatch�ah]�h]��reading a minibatch�ah]�h]�uhh#h h%hhh!h"hKaubh$)��}�(hhh]�(h))��}�(h�Putting All Things Together�h]�h/�Putting All Things Together�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j�  hhh!h"hK�ubh;)��}�(hX  Now we define the ``load_data_fashion_mnist`` function that obtains and
reads the Fashion-MNIST dataset. It returns the data iterators for both
the training set and validation set. In addition, it accepts an optional
argument to resize images to another shape.�h]�(h/�Now we define the �����}�(h�Now we define the �h j�  hhh!NhNubj,  )��}�(h�``load_data_fashion_mnist``�h]�h/�load_data_fashion_mnist�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj+  h j�  ubh/�� function that obtains and
reads the Fashion-MNIST dataset. It returns the data iterators for both
the training set and validation set. In addition, it accepts an optional
argument to resize images to another shape.�����}�(h�� function that obtains and
reads the Fashion-MNIST dataset. It returns the data iterators for both
the training set and validation set. In addition, it accepts an optional
argument to resize images to another shape.�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j�  hhubh�)��}�(hX  def load_data_fashion_mnist(batch_size, resize=None):  #@save
    """Download the Fashion-MNIST dataset and then load it into memory."""
    dataset = gluon.data.vision
    trans = [dataset.transforms.ToTensor()]
    if resize:
        trans.insert(0, dataset.transforms.Resize(resize))
    trans = dataset.transforms.Compose(trans)
    mnist_train = dataset.FashionMNIST(train=True).transform_first(trans)
    mnist_test = dataset.FashionMNIST(train=False).transform_first(trans)
    return (gluon.data.DataLoader(mnist_train, batch_size, shuffle=True,
                                  num_workers=get_dataloader_workers()),
            gluon.data.DataLoader(mnist_test, batch_size, shuffle=False,
                                  num_workers=get_dataloader_workers()))�h]�h/X  def load_data_fashion_mnist(batch_size, resize=None):  #@save
    """Download the Fashion-MNIST dataset and then load it into memory."""
    dataset = gluon.data.vision
    trans = [dataset.transforms.ToTensor()]
    if resize:
        trans.insert(0, dataset.transforms.Resize(resize))
    trans = dataset.transforms.Compose(trans)
    mnist_train = dataset.FashionMNIST(train=True).transform_first(trans)
    mnist_test = dataset.FashionMNIST(train=False).transform_first(trans)
    return (gluon.data.DataLoader(mnist_train, batch_size, shuffle=True,
                                  num_workers=get_dataloader_workers()),
            gluon.data.DataLoader(mnist_test, batch_size, shuffle=False,
                                  num_workers=get_dataloader_workers()))�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK�h j�  hhubh;)��}�(h�{Below we test the image resizing feature of the
``load_data_fashion_mnist`` function by specifying the ``resize``
argument.�h]�(h/�0Below we test the image resizing feature of the
�����}�(h�0Below we test the image resizing feature of the
�h j�  hhh!NhNubj,  )��}�(h�``load_data_fashion_mnist``�h]�h/�load_data_fashion_mnist�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj+  h j�  ubh/� function by specifying the �����}�(h� function by specifying the �h j�  hhh!NhNubj,  )��}�(h�
``resize``�h]�h/�resize�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj+  h j�  ubh/�

argument.�����}�(h�

argument.�h j�  hhh!NhNubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j�  hhubh�)��}�(h��train_iter, test_iter = load_data_fashion_mnist(32, resize=64)
for X, y in train_iter:
    print(X.shape, X.dtype, y.shape, y.dtype)
    break�h]�h/��train_iter, test_iter = load_data_fashion_mnist(32, resize=64)
for X, y in train_iter:
    print(X.shape, X.dtype, y.shape, y.dtype)
    break�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]��force���highlight_args�}�h�h�h��python�uhh�h!h"hK�h j�  hhubh;)��}�(h�TWe are now ready to work with the Fashion-MNIST dataset in the sections
that follow.�h]�h/�TWe are now ready to work with the Fashion-MNIST dataset in the sections
that follow.�����}�(hj  h j  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j�  hhubeh}�(h]��putting-all-things-together�ah]�h]��putting all things together�ah]�h]�uhh#h h%hhh!h"hK�ubh$)��}�(hhh]�(h))��}�(h�Summary�h]�h/�Summary�����}�(hj'  h j%  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j"  hhh!h"hK�ubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h��Fashion-MNIST is an apparel classification dataset consisting of
images representing 10 categories. We will use this dataset in
subsequent sections and chapters to evaluate various classification
algorithms.�h]�h;)��}�(h��Fashion-MNIST is an apparel classification dataset consisting of
images representing 10 categories. We will use this dataset in
subsequent sections and chapters to evaluate various classification
algorithms.�h]�h/��Fashion-MNIST is an apparel classification dataset consisting of
images representing 10 categories. We will use this dataset in
subsequent sections and chapters to evaluate various classification
algorithms.�����}�(hj@  h j>  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j:  ubah}�(h]�h]�h]�h]�h]�uhj8  h j5  hhh!h"hNubj9  )��}�(h�}We store the shape of any image with height :math:`h` width :math:`w`
pixels as :math:`h \times w` or (:math:`h`, :math:`w`).�h]�h;)��}�(h�}We store the shape of any image with height :math:`h` width :math:`w`
pixels as :math:`h \times w` or (:math:`h`, :math:`w`).�h]�(h/�,We store the shape of any image with height �����}�(h�,We store the shape of any image with height �h jV  ubj1  )��}�(h�	:math:`h`�h]�h/�h�����}�(hhh j_  ubah}�(h]�h]�h]�h]�h]�uhj0  h jV  ubh/� width �����}�(h� width �h jV  ubj1  )��}�(h�	:math:`w`�h]�h/�w�����}�(hhh jr  ubah}�(h]�h]�h]�h]�h]�uhj0  h jV  ubh/�
pixels as �����}�(h�
pixels as �h jV  ubj1  )��}�(h�:math:`h \times w`�h]�h/�
h \times w�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj0  h jV  ubh/� or (�����}�(h� or (�h jV  ubj1  )��}�(h�	:math:`h`�h]�h/�h�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj0  h jV  ubh/�, �����}�(h�, �h jV  ubj1  )��}�(h�	:math:`w`�h]�h/�w�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhj0  h jV  ubh/�).�����}�(h�).�h jV  ubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h jR  ubah}�(h]�h]�h]�h]�h]�uhj8  h j5  hhh!h"hNubj9  )��}�(h��Data iterators are a key component for efficient performance. Rely on
well-implemented data iterators that exploit high-performance
computing to avoid slowing down your training loop.
�h]�h;)��}�(h��Data iterators are a key component for efficient performance. Rely on
well-implemented data iterators that exploit high-performance
computing to avoid slowing down your training loop.�h]�h/��Data iterators are a key component for efficient performance. Rely on
well-implemented data iterators that exploit high-performance
computing to avoid slowing down your training loop.�����}�(hj�  h j�  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j�  ubah}�(h]�h]�h]�h]�h]�uhj8  h j5  hhh!h"hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj3  h!h"hK�h j"  hhubeh}�(h]��summary�ah]�h]��summary�ah]�h]�uhh#h h%hhh!h"hK�ubh$)��}�(hhh]�(h))��}�(h�	Exercises�h]�h/�	Exercises�����}�(hj�  h j�  hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h j�  hhh!h"hK�ubh	�enumerated_list���)��}�(hhh]�(j9  )��}�(h�UDoes reducing the ``batch_size`` (for instance, to 1) affect the
reading performance?�h]�h;)��}�(h�UDoes reducing the ``batch_size`` (for instance, to 1) affect the
reading performance?�h]�(h/�Does reducing the �����}�(h�Does reducing the �h j  ubj,  )��}�(h�``batch_size``�h]�h/�
batch_size�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhj+  h j  ubh/�5 (for instance, to 1) affect the
reading performance?�����}�(h�5 (for instance, to 1) affect the
reading performance?�h j  ubeh}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j  ubah}�(h]�h]�h]�h]�h]�uhj8  h j  hhh!h"hNubj9  )��}�(h��The data iterator performance is important. Do you think the current
implementation is fast enough? Explore various options to improve it.�h]�h;)��}�(h��The data iterator performance is important. Do you think the current
implementation is fast enough? Explore various options to improve it.�h]�h/��The data iterator performance is important. Do you think the current
implementation is fast enough? Explore various options to improve it.�����}�(hj:  h j8  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j4  ubah}�(h]�h]�h]�h]�h]�uhj8  h j  hhh!h"hNubj9  )��}�(h�ZCheck out the framework’s online API documentation. Which other
datasets are available?
�h]�h;)��}�(h�YCheck out the framework’s online API documentation. Which other
datasets are available?�h]�h/�YCheck out the framework’s online API documentation. Which other
datasets are available?�����}�(hjR  h jP  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h jL  ubah}�(h]�h]�h]�h]�h]�uhj8  h j  hhh!h"hNubeh}�(h]�h]�h]�h]�h]��enumtype��arabic��prefix�h�suffix��.�uhj  h j�  hhh!h"hK�ubh;)��}�(h�-`Discussions <https://discuss.d2l.ai/t/48>`__�h]�h	�	reference���)��}�(hjq  h]�h/�Discussions�����}�(h�Discussions�h ju  ubah}�(h]�h]�h]�h]�h]��name�j|  �refuri��https://discuss.d2l.ai/t/48�uhjs  h jo  ubah}�(h]�h]�h]�h]�h]�uhh:h!h"hK�h j�  hhubeh}�(h]��	exercises�ah]�h]��	exercises�ah]�h]�uhh#h h%hhh!h"hK�ubeh}�(h]�(� the-image-classification-dataset�heh]�h]�(� the image classification dataset��sec_fashion_mnist�eh]�h]�uhh#h hhhh!h"hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h"uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h(N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h"�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}�(�lecun.bottou.bengio.ea.1998�]�h	�citation_reference���)��}�(h�[LeCun.Bottou.Bengio.ea.1998]_�h]�h/�LeCun.Bottou.Bengio.ea.1998�����}�(hhh j  ubah}�(h]�h\ah]�h^ah]�h]�h]��refname�j�  uhj  h h<uba�xiao.rasul.vollgraf.2017�]�j  )��}�(h�[Xiao.Rasul.Vollgraf.2017]_�h]�h/�Xiao.Rasul.Vollgraf.2017�����}�(hhh j  ubah}�(h]�h�ah]�h^ah]�h]�h]��refname�j  uhj  h h<ubau�refids�}�h]�has�nameids�}�(j�  hj�  j�  j  j  j}  jz  j  j  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  Nj  Nj}  Nj  Nj�  Nj�  Nuh}�(hh%j�  h%h\j  h�j  j  h�jz  j  j  j�  j�  j"  j�  j�  u�footnote_refs�}��citation_refs�}�(j�  ]�j  aj  ]�j  au�autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]��transform_messages�]�h	�system_message���)��}�(hhh]�h;)��}�(hhh]�h/�7Hyperlink target "sec-fashion-mnist" is not referenced.�����}�(hhh jO  ubah}�(h]�h]�h]�h]�h]�uhh:h jL  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h"�line�KuhjJ  uba�transformer�N�
decoration�Nhhub.