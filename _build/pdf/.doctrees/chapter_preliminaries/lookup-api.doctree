��r6      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Documentation�h]�h	�Text����Documentation�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�B/home/mn/sa20/sam2/_build/rst/chapter_preliminaries/lookup-api.rst�hKubh	�	paragraph���)��}�(hXU  Due to constraints on the length of this book, we cannot possibly
introduce every single MXNet function and class (and you probably would
not want us to). The API documentation and additional tutorials and
examples provide plenty of documentation beyond the book. In this
section we provide you with some guidance to exploring the MXNet API.�h]�hXU  Due to constraints on the length of this book, we cannot possibly
introduce every single MXNet function and class (and you probably would
not want us to). The API documentation and additional tutorials and
examples provide plenty of documentation beyond the book. In this
section we provide you with some guidance to exploring the MXNet API.�����}�(hh0hh.hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh)��}�(hhh]�(h)��}�(h�1Finding All the Functions and Classes in a Module�h]�h�1Finding All the Functions and Classes in a Module�����}�(hhAhh?hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh<hhhh+hKubh-)��}�(h��In order to know which functions and classes can be called in a module,
we invoke the ``dir`` function. For instance, we can query all
properties in the module for generating random numbers:�h]�(h�VIn order to know which functions and classes can be called in a module,
we invoke the �����}�(h�VIn order to know which functions and classes can be called in a module,
we invoke the �hhMhhhNhNubh	�literal���)��}�(h�``dir``�h]�h�dir�����}�(hhhhXubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhhMubh�a function. For instance, we can query all
properties in the module for generating random numbers:�����}�(h�a function. For instance, we can query all
properties in the module for generating random numbers:�hhMhhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhh<hhubh	�literal_block���)��}�(h�*from mxnet import np
print(dir(np.random))�h]�h�*from mxnet import np
print(dir(np.random))�����}�(hhhhsubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}��	xml:space��preserve��language��python�uh*hqhh+hKhh<hhubh-)��}�(hX�  Generally, we can ignore functions that start and end with ``__``
(special objects in Python) or functions that start with a single
``_``\ (usually internal functions). Based on the remaining function or
attribute names, we might hazard a guess that this module offers various
methods for generating random numbers, including sampling from the
uniform distribution (``uniform``), normal distribution (``normal``),
and multinomial distribution (``multinomial``).�h]�(h�;Generally, we can ignore functions that start and end with �����}�(h�;Generally, we can ignore functions that start and end with �hh�hhhNhNubhW)��}�(h�``__``�h]�h�__�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhh�ubh�C
(special objects in Python) or functions that start with a single
�����}�(h�C
(special objects in Python) or functions that start with a single
�hh�hhhNhNubhW)��}�(h�``_``�h]�h�_�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhh�ubh��  (usually internal functions). Based on the remaining function or
attribute names, we might hazard a guess that this module offers various
methods for generating random numbers, including sampling from the
uniform distribution (�����}�(h��\ (usually internal functions). Based on the remaining function or
attribute names, we might hazard a guess that this module offers various
methods for generating random numbers, including sampling from the
uniform distribution (�hh�hhhNhNubhW)��}�(h�``uniform``�h]�h�uniform�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhh�ubh�), normal distribution (�����}�(h�), normal distribution (�hh�hhhNhNubhW)��}�(h�
``normal``�h]�h�normal�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhh�ubh�!),
and multinomial distribution (�����}�(h�!),
and multinomial distribution (�hh�hhhNhNubhW)��}�(h�``multinomial``�h]�h�multinomial�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhh�ubh�).�����}�(h�).�hh�hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhh<hhubeh}�(h ]��1finding-all-the-functions-and-classes-in-a-module�ah"]�h$]��1finding all the functions and classes in a module�ah&]�h(]�uh*h
hhhhhh+hKubh)��}�(hhh]�(h)��}�(h�3Finding the Usage of Specific Functions and Classes�h]�h�3Finding the Usage of Specific Functions and Classes�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh�hhhh+hK ubh-)��}�(h��For more specific instructions on how to use a given function or class,
we can invoke the ``help`` function. As an example, let us explore the
usage instructions for tensors’ ``ones`` function.�h]�(h�ZFor more specific instructions on how to use a given function or class,
we can invoke the �����}�(h�ZFor more specific instructions on how to use a given function or class,
we can invoke the �hj  hhhNhNubhW)��}�(h�``help``�h]�h�help�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj  ubh�O function. As an example, let us explore the
usage instructions for tensors’ �����}�(h�O function. As an example, let us explore the
usage instructions for tensors’ �hj  hhhNhNubhW)��}�(h�``ones``�h]�h�ones�����}�(hhhj+  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj  ubh�
 function.�����}�(h�
 function.�hj  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK"hh�hhubhr)��}�(h�help(np.ones)�h]�h�help(np.ones)�����}�(hhhjD  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��python�uh*hqhh+hK&hh�hhubh-)��}�(h��From the documentation, we can see that the ``ones`` function creates a
new tensor with the specified shape and sets all the elements to the
value of 1. Whenever possible, you should run a quick test to confirm
your interpretation:�h]�(h�,From the documentation, we can see that the �����}�(h�,From the documentation, we can see that the �hjV  hhhNhNubhW)��}�(h�``ones``�h]�h�ones�����}�(hhhj_  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhjV  ubh�� function creates a
new tensor with the specified shape and sets all the elements to the
value of 1. Whenever possible, you should run a quick test to confirm
your interpretation:�����}�(h�� function creates a
new tensor with the specified shape and sets all the elements to the
value of 1. Whenever possible, you should run a quick test to confirm
your interpretation:�hjV  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK*hh�hhubhr)��}�(h�
np.ones(4)�h]�h�
np.ones(4)�����}�(hhhjx  ubah}�(h ]�h"]�h$]�h&]�h(]��force���highlight_args�}�h�h�h��python�uh*hqhh+hK/hh�hhubh-)��}�(hXS  In the Jupyter notebook, we can use ``?`` to display the document in
another window. For example, ``list?`` will create content that is
almost identical to ``help(list)``, displaying it in a new browser
window. In addition, if we use two question marks, such as ``list??``,
the Python code implementing the function will also be displayed.�h]�(h�$In the Jupyter notebook, we can use �����}�(h�$In the Jupyter notebook, we can use �hj�  hhhNhNubhW)��}�(h�``?``�h]�h�?�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj�  ubh�9 to display the document in
another window. For example, �����}�(h�9 to display the document in
another window. For example, �hj�  hhhNhNubhW)��}�(h�	``list?``�h]�h�list?�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj�  ubh�1 will create content that is
almost identical to �����}�(h�1 will create content that is
almost identical to �hj�  hhhNhNubhW)��}�(h�``help(list)``�h]�h�
help(list)�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj�  ubh�\, displaying it in a new browser
window. In addition, if we use two question marks, such as �����}�(h�\, displaying it in a new browser
window. In addition, if we use two question marks, such as �hj�  hhhNhNubhW)��}�(h�
``list??``�h]�h�list??�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj�  ubh�C,
the Python code implementing the function will also be displayed.�����}�(h�C,
the Python code implementing the function will also be displayed.�hj�  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK3hh�hhubeh}�(h ]��3finding-the-usage-of-specific-functions-and-classes�ah"]�h$]��3finding the usage of specific functions and classes�ah&]�h(]�uh*h
hhhhhh+hK ubh)��}�(hhh]�(h)��}�(h�Summary�h]�h�Summary�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK:ubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h�bThe official documentation provides plenty of descriptions and
examples that are beyond this book.�h]�h-)��}�(h�bThe official documentation provides plenty of descriptions and
examples that are beyond this book.�h]�h�bThe official documentation provides plenty of descriptions and
examples that are beyond this book.�����}�(hj  hj	  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK<hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj   hhhh+hNubj  )��}�(h��We can look up documentation for the usage of an API by calling the
``dir`` and ``help`` functions, or ``?`` and ``??`` in Jupyter
notebooks.
�h]�h-)��}�(h��We can look up documentation for the usage of an API by calling the
``dir`` and ``help`` functions, or ``?`` and ``??`` in Jupyter
notebooks.�h]�(h�DWe can look up documentation for the usage of an API by calling the
�����}�(h�DWe can look up documentation for the usage of an API by calling the
�hj!  ubhW)��}�(h�``dir``�h]�h�dir�����}�(hhhj*  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj!  ubh� and �����}�(h� and �hj!  ubhW)��}�(h�``help``�h]�h�help�����}�(hhhj=  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj!  ubh� functions, or �����}�(h� functions, or �hj!  ubhW)��}�(h�``?``�h]�h�?�����}�(hhhjP  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj!  ubh� and �����}�(hj<  hj!  ubhW)��}�(h�``??``�h]�h�??�����}�(hhhjb  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hVhj!  ubh� in Jupyter
notebooks.�����}�(h� in Jupyter
notebooks.�hj!  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK>hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj   hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]��bullet��-�uh*j�  hh+hK<hj�  hhubeh}�(h ]��summary�ah"]�h$]��summary�ah&]�h(]�uh*h
hhhhhh+hK:ubh)��}�(hhh]�(h)��}�(h�	Exercises�h]�h�	Exercises�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKCubh	�enumerated_list���)��}�(hhh]�j  )��}�(h��Look up the documentation for any function or class in the deep
learning framework. Can you also find the documentation on the
official website of the framework?
�h]�h-)��}�(h��Look up the documentation for any function or class in the deep
learning framework. Can you also find the documentation on the
official website of the framework?�h]�h��Look up the documentation for any function or class in the deep
learning framework. Can you also find the documentation on the
official website of the framework?�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKEhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*j  hj�  hhhh+hNubah}�(h ]�h"]�h$]�h&]�h(]��enumtype��arabic��prefix�h�suffix��.�uh*j�  hj�  hhhh+hKEubh-)��}�(h�-`Discussions <https://discuss.d2l.ai/t/38>`__�h]�h	�	reference���)��}�(hj�  h]�h�Discussions�����}�(h�Discussions�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]��name�j�  �refuri��https://discuss.d2l.ai/t/38�uh*j�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKIhj�  hhubeh}�(h ]��	exercises�ah"]�h$]��	exercises�ah&]�h(]�uh*h
hhhhhh+hKCubeh}�(h ]��documentation�ah"]�h$]��documentation�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  h�h�j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j�  Nh�Nj�  Nj�  Nj�  Nuh }�(j�  hh�h<j�  h�j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.