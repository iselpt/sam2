\selectlanguage *{english}
\contentsline {chapter}{Preface}{1}{chapter*.3}%
\contentsline {chapter}{Installation}{9}{chapter*.4}%
\contentsline {chapter}{Notation}{13}{chapter*.5}%
\contentsline {chapter}{\numberline {1}Introduction}{17}{chapter.1}%
\contentsline {section}{\numberline {1.1}A Motivating Example}{18}{section.1.1}%
\contentsline {section}{\numberline {1.2}The Key Components: Data, Models, and Algorithms}{20}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Data}{21}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Models}{22}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Objective functions}{22}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Optimization algorithms}{23}{subsection.1.2.4}%
\contentsline {section}{\numberline {1.3}Kinds of Machine Learning}{23}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Supervised learning}{23}{subsection.1.3.1}%
\contentsline {subsubsection}{Regression}{24}{subsubsection*.6}%
\contentsline {subsubsection}{Classification}{26}{subsubsection*.7}%
\contentsline {subsubsection}{Tagging}{28}{subsubsection*.8}%
\contentsline {subsubsection}{Search and ranking}{29}{subsubsection*.9}%
\contentsline {subsubsection}{Recommender systems}{29}{subsubsection*.10}%
\contentsline {subsubsection}{Sequence Learning}{30}{subsubsection*.11}%
\contentsline {subsection}{\numberline {1.3.2}Unsupervised learning}{32}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}Interacting with an Environment}{33}{subsection.1.3.3}%
\contentsline {subsection}{\numberline {1.3.4}Reinforcement learning}{34}{subsection.1.3.4}%
\contentsline {subsubsection}{MDPs, bandits, and friends}{35}{subsubsection*.12}%
\contentsline {section}{\numberline {1.4}Roots}{35}{section.1.4}%
\contentsline {section}{\numberline {1.5}The Road to Deep Learning}{37}{section.1.5}%
\contentsline {section}{\numberline {1.6}Success Stories}{39}{section.1.6}%
\setcounter {tocdepth}{2}
\contentsline {chapter}{\numberline {2}Preliminaries}{43}{chapter.2}%
\contentsline {section}{\numberline {2.1}Data Manipulation}{43}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Getting Started}{44}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Operations}{45}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Broadcasting Mechanism}{47}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Indexing and Slicing}{47}{subsection.2.1.4}%
\contentsline {subsection}{\numberline {2.1.5}Saving Memory}{48}{subsection.2.1.5}%
\contentsline {subsection}{\numberline {2.1.6}Conversion to Other Python Objects}{48}{subsection.2.1.6}%
\contentsline {section}{\numberline {2.2}Data Preprocessing}{49}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Reading the Dataset}{49}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Handling Missing Data}{50}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Conversion to the Tensor Format}{51}{subsection.2.2.3}%
\contentsline {section}{\numberline {2.3}Linear Algebra}{51}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Scalars}{51}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Vectors}{52}{subsection.2.3.2}%
\contentsline {subsubsection}{Length, Dimensionality, and Shape}{53}{subsubsection*.13}%
\contentsline {subsection}{\numberline {2.3.3}Matrices}{53}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Tensors}{54}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}Basic Properties of Tensor Arithmetic}{55}{subsection.2.3.5}%
\contentsline {subsection}{\numberline {2.3.6}Reduction}{55}{subsection.2.3.6}%
\contentsline {subsubsection}{Non\sphinxhyphen {}Reduction Sum}{56}{subsubsection*.14}%
\contentsline {subsection}{\numberline {2.3.7}Dot Products}{57}{subsection.2.3.7}%
\contentsline {subsection}{\numberline {2.3.8}Matrix\sphinxhyphen {}Vector Products}{57}{subsection.2.3.8}%
\contentsline {subsection}{\numberline {2.3.9}Matrix\sphinxhyphen {}Matrix Multiplication}{58}{subsection.2.3.9}%
\contentsline {subsection}{\numberline {2.3.10}Norms}{59}{subsection.2.3.10}%
\contentsline {subsubsection}{Norms and Objectives}{60}{subsubsection*.15}%
\contentsline {subsection}{\numberline {2.3.11}More on Linear Algebra}{60}{subsection.2.3.11}%
\contentsline {section}{\numberline {2.4}Calculus}{61}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Derivatives and Differentiation}{62}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Partial Derivatives}{65}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}Gradients}{66}{subsection.2.4.3}%
\contentsline {subsection}{\numberline {2.4.4}Chain Rule}{66}{subsection.2.4.4}%
\contentsline {section}{\numberline {2.5}Automatic Differentiation}{67}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}A Simple Example}{67}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Backward for Non\sphinxhyphen {}Scalar Variables}{68}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Detaching Computation}{69}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Computing the Gradient of Python Control Flow}{69}{subsection.2.5.4}%
\contentsline {section}{\numberline {2.6}Probability}{70}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Basic Probability Theory}{72}{subsection.2.6.1}%
\contentsline {subsubsection}{Axioms of Probability Theory}{73}{subsubsection*.16}%
\contentsline {subsubsection}{Random Variables}{74}{subsubsection*.17}%
\contentsline {subsection}{\numberline {2.6.2}Dealing with Multiple Random Variables}{74}{subsection.2.6.2}%
\contentsline {subsubsection}{Joint Probability}{75}{subsubsection*.18}%
\contentsline {subsubsection}{Conditional Probability}{75}{subsubsection*.19}%
\contentsline {subsubsection}{Bayes’ theorem}{75}{subsubsection*.20}%
\contentsline {subsubsection}{Marginalization}{75}{subsubsection*.21}%
\contentsline {subsubsection}{Independence}{75}{subsubsection*.22}%
\contentsline {subsubsection}{Application}{76}{subsubsection*.23}%
\contentsline {subsection}{\numberline {2.6.3}Expectation and Variance}{77}{subsection.2.6.3}%
\contentsline {section}{\numberline {2.7}Documentation}{78}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Finding All the Functions and Classes in a Module}{78}{subsection.2.7.1}%
\contentsline {subsection}{\numberline {2.7.2}Finding the Usage of Specific Functions and Classes}{79}{subsection.2.7.2}%
\contentsline {chapter}{\numberline {3}Linear Neural Networks}{81}{chapter.3}%
\contentsline {section}{\numberline {3.1}Linear Regression}{81}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Basic Elements of Linear Regression}{81}{subsection.3.1.1}%
\contentsline {subsubsection}{Linear Model}{82}{subsubsection*.24}%
\contentsline {subsubsection}{Loss Function}{83}{subsubsection*.25}%
\contentsline {subsubsection}{Analytic Solution}{84}{subsubsection*.26}%
\contentsline {subsubsection}{Minibatch Stochastic Gradient Descent}{84}{subsubsection*.27}%
\contentsline {subsubsection}{Making Predictions with the Learned Model}{85}{subsubsection*.28}%
\contentsline {subsection}{\numberline {3.1.2}Vectorization for Speed}{85}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}The Normal Distribution and Squared Loss}{87}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}From Linear Regression to Deep Networks}{88}{subsection.3.1.4}%
\contentsline {subsubsection}{Neural Network Diagram}{88}{subsubsection*.29}%
\contentsline {subsubsection}{Biology}{89}{subsubsection*.30}%
\contentsline {section}{\numberline {3.2}Linear Regression Implementation from Scratch}{91}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Generating the Dataset}{91}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Reading the Dataset}{92}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Initializing Model Parameters}{93}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Defining the Model}{93}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}Defining the Loss Function}{93}{subsection.3.2.5}%
\contentsline {subsection}{\numberline {3.2.6}Defining the Optimization Algorithm}{94}{subsection.3.2.6}%
\contentsline {subsection}{\numberline {3.2.7}Training}{94}{subsection.3.2.7}%
\contentsline {section}{\numberline {3.3}Concise Implementation of Linear Regression}{96}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Generating the Dataset}{96}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Reading the Dataset}{96}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Defining the Model}{97}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Initializing Model Parameters}{98}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Defining the Loss Function}{98}{subsection.3.3.5}%
\contentsline {subsection}{\numberline {3.3.6}Defining the Optimization Algorithm}{98}{subsection.3.3.6}%
\contentsline {subsection}{\numberline {3.3.7}Training}{99}{subsection.3.3.7}%
\contentsline {section}{\numberline {3.4}Softmax Regression}{100}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Classification Problem}{100}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Network Architecture}{101}{subsection.3.4.2}%
\contentsline {subsection}{\numberline {3.4.3}Softmax Operation}{102}{subsection.3.4.3}%
\contentsline {subsection}{\numberline {3.4.4}Vectorization for Minibatches}{102}{subsection.3.4.4}%
\contentsline {subsection}{\numberline {3.4.5}Loss Function}{103}{subsection.3.4.5}%
\contentsline {subsubsection}{Log\sphinxhyphen {}Likelihood}{103}{subsubsection*.31}%
\contentsline {subsubsection}{Softmax and Derivatives}{104}{subsubsection*.32}%
\contentsline {subsubsection}{Cross\sphinxhyphen {}Entropy Loss}{104}{subsubsection*.33}%
\contentsline {subsection}{\numberline {3.4.6}Information Theory Basics}{105}{subsection.3.4.6}%
\contentsline {subsubsection}{Entropy}{105}{subsubsection*.34}%
\contentsline {subsubsection}{Surprisal}{105}{subsubsection*.35}%
\contentsline {subsubsection}{Cross\sphinxhyphen {}Entropy Revisited}{105}{subsubsection*.36}%
\contentsline {subsection}{\numberline {3.4.7}Model Prediction and Evaluation}{106}{subsection.3.4.7}%
\contentsline {section}{\numberline {3.5}The Image Classification Dataset}{107}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}Reading the Dataset}{107}{subsection.3.5.1}%
\contentsline {subsection}{\numberline {3.5.2}Reading a Minibatch}{108}{subsection.3.5.2}%
\contentsline {subsection}{\numberline {3.5.3}Putting All Things Together}{109}{subsection.3.5.3}%
\contentsline {section}{\numberline {3.6}Implementation of Softmax Regression from Scratch}{110}{section.3.6}%
\contentsline {subsection}{\numberline {3.6.1}Initializing Model Parameters}{110}{subsection.3.6.1}%
\contentsline {subsection}{\numberline {3.6.2}Defining the Softmax Operation}{110}{subsection.3.6.2}%
\contentsline {subsection}{\numberline {3.6.3}Defining the Model}{111}{subsection.3.6.3}%
\contentsline {subsection}{\numberline {3.6.4}Defining the Loss Function}{112}{subsection.3.6.4}%
\contentsline {subsection}{\numberline {3.6.5}Classification Accuracy}{112}{subsection.3.6.5}%
\contentsline {subsection}{\numberline {3.6.6}Training}{113}{subsection.3.6.6}%
\contentsline {subsection}{\numberline {3.6.7}Prediction}{115}{subsection.3.6.7}%
\contentsline {section}{\numberline {3.7}Concise Implementation of Softmax Regression}{116}{section.3.7}%
\contentsline {subsection}{\numberline {3.7.1}Initializing Model Parameters}{117}{subsection.3.7.1}%
\contentsline {subsection}{\numberline {3.7.2}Softmax Implementation Revisited}{117}{subsection.3.7.2}%
\contentsline {subsection}{\numberline {3.7.3}Optimization Algorithm}{118}{subsection.3.7.3}%
\contentsline {subsection}{\numberline {3.7.4}Training}{118}{subsection.3.7.4}%
\contentsline {chapter}{\numberline {4}Appendix: Mathematics for Deep Learning}{119}{chapter.4}%
\contentsline {section}{\numberline {4.1}Geometry and Linear Algebraic Operations}{120}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Geometry of Vectors}{120}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Dot Products and Angles}{122}{subsection.4.1.2}%
\contentsline {subsubsection}{Cosine Similarity}{124}{subsubsection*.37}%
\contentsline {subsection}{\numberline {4.1.3}Hyperplanes}{124}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Geometry of Linear Transformations}{126}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}Linear Dependence}{128}{subsection.4.1.5}%
\contentsline {subsection}{\numberline {4.1.6}Rank}{129}{subsection.4.1.6}%
\contentsline {subsection}{\numberline {4.1.7}Invertibility}{129}{subsection.4.1.7}%
\contentsline {subsubsection}{Numerical Issues}{130}{subsubsection*.38}%
\contentsline {subsection}{\numberline {4.1.8}Determinant}{130}{subsection.4.1.8}%
\contentsline {subsection}{\numberline {4.1.9}Tensors and Common Linear Algebra Operations}{132}{subsection.4.1.9}%
\contentsline {subsubsection}{Common Examples from Linear Algebra}{132}{subsubsection*.39}%
\contentsline {subsubsection}{Expressing in Code}{133}{subsubsection*.40}%
\contentsline {section}{\numberline {4.2}Eigendecompositions}{135}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Finding Eigenvalues}{135}{subsection.4.2.1}%
\contentsline {subsubsection}{An Example}{135}{subsubsection*.41}%
\contentsline {subsection}{\numberline {4.2.2}Decomposing Matrices}{136}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Operations on Eigendecompositions}{137}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Eigendecompositions of Symmetric Matrices}{137}{subsection.4.2.4}%
\contentsline {subsection}{\numberline {4.2.5}Gershgorin Circle Theorem}{138}{subsection.4.2.5}%
\contentsline {subsection}{\numberline {4.2.6}A Useful Application: The Growth of Iterated Maps}{139}{subsection.4.2.6}%
\contentsline {subsubsection}{Eigenvectors as Long Term Behavior}{139}{subsubsection*.42}%
\contentsline {subsubsection}{Behavior on Random Data}{139}{subsubsection*.43}%
\contentsline {subsubsection}{Relating Back to Eigenvectors}{140}{subsubsection*.44}%
\contentsline {subsubsection}{An Observation}{140}{subsubsection*.45}%
\contentsline {subsubsection}{Fixing the Normalization}{141}{subsubsection*.46}%
\contentsline {subsection}{\numberline {4.2.7}Conclusions}{141}{subsection.4.2.7}%
\contentsline {section}{\numberline {4.3}Single Variable Calculus}{142}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Differential Calculus}{143}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Rules of Calculus}{145}{subsection.4.3.2}%
\contentsline {subsubsection}{Common Derivatives}{145}{subsubsection*.47}%
\contentsline {subsubsection}{Derivative Rules}{145}{subsubsection*.48}%
\contentsline {subsubsection}{Linear Approximation}{147}{subsubsection*.49}%
\contentsline {subsubsection}{Higher Order Derivatives}{148}{subsubsection*.50}%
\contentsline {subsubsection}{Taylor Series}{150}{subsubsection*.51}%
\contentsline {section}{\numberline {4.4}Multivariable Calculus}{152}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Higher\sphinxhyphen {}Dimensional Differentiation}{152}{subsection.4.4.1}%
\contentsline {subsection}{\numberline {4.4.2}Geometry of Gradients and Gradient Descent}{153}{subsection.4.4.2}%
\contentsline {subsection}{\numberline {4.4.3}A Note on Mathematical Optimization}{154}{subsection.4.4.3}%
\contentsline {subsection}{\numberline {4.4.4}Multivariate Chain Rule}{155}{subsection.4.4.4}%
\contentsline {subsection}{\numberline {4.4.5}The Backpropagation Algorithm}{157}{subsection.4.4.5}%
\contentsline {subsection}{\numberline {4.4.6}Hessians}{159}{subsection.4.4.6}%
\contentsline {subsection}{\numberline {4.4.7}A Little Matrix Calculus}{161}{subsection.4.4.7}%
\contentsline {section}{\numberline {4.5}Integral Calculus}{166}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Geometric Interpretation}{166}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}The Fundamental Theorem of Calculus}{167}{subsection.4.5.2}%
\contentsline {subsection}{\numberline {4.5.3}Change of Variables}{169}{subsection.4.5.3}%
\contentsline {subsection}{\numberline {4.5.4}A Comment on Sign Conventions}{170}{subsection.4.5.4}%
\contentsline {subsection}{\numberline {4.5.5}Multiple Integrals}{171}{subsection.4.5.5}%
\contentsline {subsection}{\numberline {4.5.6}Change of Variables in Multiple Integrals}{173}{subsection.4.5.6}%
\contentsline {section}{\numberline {4.6}Random Variables}{174}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Continuous Random Variables}{174}{subsection.4.6.1}%
\contentsline {subsubsection}{From Discrete to Continuous}{174}{subsubsection*.52}%
\contentsline {subsubsection}{Probability Density Functions}{176}{subsubsection*.53}%
\contentsline {subsubsection}{Cumulative Distribution Functions}{177}{subsubsection*.54}%
\contentsline {subsubsection}{Means}{178}{subsubsection*.55}%
\contentsline {subsubsection}{Variances}{178}{subsubsection*.56}%
\contentsline {subsubsection}{Standard Deviations}{179}{subsubsection*.57}%
\contentsline {subsubsection}{Means and Variances in the Continuum}{181}{subsubsection*.58}%
\contentsline {subsubsection}{Joint Density Functions}{183}{subsubsection*.59}%
\contentsline {subsubsection}{Marginal Distributions}{183}{subsubsection*.60}%
\contentsline {subsubsection}{Covariance}{184}{subsubsection*.61}%
\contentsline {subsubsection}{Correlation}{186}{subsubsection*.62}%
\contentsline {section}{\numberline {4.7}Maximum Likelihood}{189}{section.4.7}%
\contentsline {subsection}{\numberline {4.7.1}The Maximum Likelihood Principle}{189}{subsection.4.7.1}%
\contentsline {subsubsection}{A Concrete Example}{189}{subsubsection*.63}%
\contentsline {subsection}{\numberline {4.7.2}Numerical Optimization and the Negative Log\sphinxhyphen {}Likelihood}{190}{subsection.4.7.2}%
\contentsline {subsection}{\numberline {4.7.3}Maximum Likelihood for Continuous Variables}{192}{subsection.4.7.3}%
\contentsline {section}{\numberline {4.8}Distributions}{193}{section.4.8}%
\contentsline {subsection}{\numberline {4.8.1}Bernoulli}{194}{subsection.4.8.1}%
\contentsline {subsection}{\numberline {4.8.2}Discrete Uniform}{194}{subsection.4.8.2}%
\contentsline {subsection}{\numberline {4.8.3}Continuous Uniform}{195}{subsection.4.8.3}%
\contentsline {subsection}{\numberline {4.8.4}Binomial}{196}{subsection.4.8.4}%
\contentsline {subsection}{\numberline {4.8.5}Poisson}{197}{subsection.4.8.5}%
\contentsline {subsection}{\numberline {4.8.6}Gaussian}{199}{subsection.4.8.6}%
\contentsline {subsection}{\numberline {4.8.7}Exponential Family}{201}{subsection.4.8.7}%
\contentsline {section}{\numberline {4.9}Naive Bayes}{202}{section.4.9}%
\contentsline {subsection}{\numberline {4.9.1}Optical Character Recognition}{203}{subsection.4.9.1}%
\contentsline {subsection}{\numberline {4.9.2}The Probabilistic Model for Classification}{204}{subsection.4.9.2}%
\contentsline {subsection}{\numberline {4.9.3}The Naive Bayes Classifier}{204}{subsection.4.9.3}%
\contentsline {subsection}{\numberline {4.9.4}Training}{205}{subsection.4.9.4}%
\contentsline {section}{\numberline {4.10}Statistics}{208}{section.4.10}%
\contentsline {subsection}{\numberline {4.10.1}Evaluating and Comparing Estimators}{208}{subsection.4.10.1}%
\contentsline {subsubsection}{Mean Squared Error}{209}{subsubsection*.64}%
\contentsline {subsubsection}{Statistical Bias}{209}{subsubsection*.65}%
\contentsline {subsubsection}{Variance and Standard Deviation}{210}{subsubsection*.66}%
\contentsline {subsubsection}{The Bias\sphinxhyphen {}Variance Trade\sphinxhyphen {}off}{210}{subsubsection*.67}%
\contentsline {subsubsection}{Evaluating Estimators in Code}{211}{subsubsection*.68}%
\contentsline {subsection}{\numberline {4.10.2}Conducting Hypothesis Tests}{211}{subsection.4.10.2}%
\contentsline {subsubsection}{Statistical Significance}{212}{subsubsection*.69}%
\contentsline {subsubsection}{Statistical Power}{213}{subsubsection*.70}%
\contentsline {subsubsection}{Test Statistic}{214}{subsubsection*.71}%
\contentsline {subsubsection}{\(p\)\sphinxhyphen {}value}{214}{subsubsection*.72}%
\contentsline {subsubsection}{One\sphinxhyphen {}side Test and Two\sphinxhyphen {}sided Test}{214}{subsubsection*.73}%
\contentsline {subsubsection}{General Steps of Hypothesis Testing}{214}{subsubsection*.74}%
\contentsline {subsection}{\numberline {4.10.3}Constructing Confidence Intervals}{215}{subsection.4.10.3}%
\contentsline {subsubsection}{Definition}{215}{subsubsection*.75}%
\contentsline {subsubsection}{Interpretation}{215}{subsubsection*.76}%
\contentsline {subsubsection}{A Gaussian Example}{216}{subsubsection*.77}%
\contentsline {section}{\numberline {4.11}Information Theory}{218}{section.4.11}%
\contentsline {subsection}{\numberline {4.11.1}Information}{218}{subsection.4.11.1}%
\contentsline {subsubsection}{Self\sphinxhyphen {}information}{219}{subsubsection*.78}%
\contentsline {subsection}{\numberline {4.11.2}Entropy}{220}{subsection.4.11.2}%
\contentsline {subsubsection}{Motivating Entropy}{220}{subsubsection*.79}%
\contentsline {subsubsection}{Definition}{220}{subsubsection*.80}%
\contentsline {subsubsection}{Interpretations}{221}{subsubsection*.81}%
\contentsline {subsubsection}{Properties of Entropy}{221}{subsubsection*.82}%
\contentsline {subsection}{\numberline {4.11.3}Mutual Information}{222}{subsection.4.11.3}%
\contentsline {subsubsection}{Joint Entropy}{222}{subsubsection*.83}%
\contentsline {subsubsection}{Conditional Entropy}{223}{subsubsection*.84}%
\contentsline {subsubsection}{Mutual Information}{224}{subsubsection*.85}%
\contentsline {subsubsection}{Properties of Mutual Information}{225}{subsubsection*.86}%
\contentsline {subsubsection}{Pointwise Mutual Information}{225}{subsubsection*.87}%
\contentsline {subsubsection}{Applications of Mutual Information}{226}{subsubsection*.88}%
\contentsline {subsection}{\numberline {4.11.4}Kullback–Leibler Divergence}{226}{subsection.4.11.4}%
\contentsline {subsubsection}{Definition}{226}{subsubsection*.89}%
\contentsline {subsubsection}{KL Divergence Properties}{227}{subsubsection*.90}%
\contentsline {subsubsection}{Example}{227}{subsubsection*.91}%
\contentsline {subsection}{\numberline {4.11.5}Cross Entropy}{228}{subsection.4.11.5}%
\contentsline {subsubsection}{Formal Definition}{229}{subsubsection*.92}%
\contentsline {subsubsection}{Properties}{229}{subsubsection*.93}%
\contentsline {subsubsection}{Cross Entropy as An Objective Function of Multi\sphinxhyphen {}class Classification}{229}{subsubsection*.94}%
\contentsline {chapter}{\numberline {5}Appendix: Tools for Deep Learning}{233}{chapter.5}%
\contentsline {section}{\numberline {5.1}Using Jupyter}{233}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Editing and Running the Code Locally}{233}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Advanced Options}{237}{subsection.5.1.2}%
\contentsline {subsubsection}{Markdown Files in Jupyter}{237}{subsubsection*.95}%
\contentsline {subsubsection}{Running Jupyter Notebook on a Remote Server}{237}{subsubsection*.96}%
\contentsline {subsubsection}{Timing}{238}{subsubsection*.97}%
\contentsline {section}{\numberline {5.2}Using Amazon SageMaker}{238}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Registering and Logging In}{238}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Creating a SageMaker Instance}{239}{subsection.5.2.2}%
\contentsline {subsection}{\numberline {5.2.3}Running and Stopping an Instance}{240}{subsection.5.2.3}%
\contentsline {subsection}{\numberline {5.2.4}Updating Notebooks}{241}{subsection.5.2.4}%
\contentsline {section}{\numberline {5.3}Using AWS EC2 Instances}{242}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Creating and Running an EC2 Instance}{242}{subsection.5.3.1}%
\contentsline {subsubsection}{Presetting Location}{243}{subsubsection*.98}%
\contentsline {subsubsection}{Increasing Limits}{243}{subsubsection*.99}%
\contentsline {subsubsection}{Launching Instance}{244}{subsubsection*.100}%
\contentsline {subsubsection}{Connecting to the Instance}{246}{subsubsection*.101}%
\contentsline {subsection}{\numberline {5.3.2}Installing CUDA}{247}{subsection.5.3.2}%
\contentsline {subsection}{\numberline {5.3.3}Installing MXNet and Downloading the D2L Notebooks}{248}{subsection.5.3.3}%
\contentsline {subsection}{\numberline {5.3.4}Running Jupyter}{249}{subsection.5.3.4}%
\contentsline {subsection}{\numberline {5.3.5}Closing Unused Instances}{250}{subsection.5.3.5}%
\contentsline {section}{\numberline {5.4}Using Google Colab}{250}{section.5.4}%
\contentsline {section}{\numberline {5.5}Selecting Servers and GPUs}{251}{section.5.5}%
\contentsline {subsection}{\numberline {5.5.1}Selecting Servers}{252}{subsection.5.5.1}%
\contentsline {subsection}{\numberline {5.5.2}Selecting GPUs}{253}{subsection.5.5.2}%
\contentsline {section}{\numberline {5.6}Contributing to This Book}{256}{section.5.6}%
\contentsline {subsection}{\numberline {5.6.1}Minor Text Changes}{256}{subsection.5.6.1}%
\contentsline {subsection}{\numberline {5.6.2}Propose a Major Change}{256}{subsection.5.6.2}%
\contentsline {subsection}{\numberline {5.6.3}Adding a New Section or a New Framework Implementation}{257}{subsection.5.6.3}%
\contentsline {subsection}{\numberline {5.6.4}Submitting a Major Change}{257}{subsection.5.6.4}%
\contentsline {subsubsection}{Installing Git}{258}{subsubsection*.102}%
\contentsline {subsubsection}{Logging in to GitHub}{258}{subsubsection*.103}%
\contentsline {subsubsection}{Cloning the Repository}{258}{subsubsection*.104}%
\contentsline {subsubsection}{Editing the Book and Push}{259}{subsubsection*.105}%
\contentsline {subsubsection}{Pull Request}{260}{subsubsection*.106}%
\contentsline {subsubsection}{Submitting Pull Request}{260}{subsubsection*.107}%
\contentsline {section}{\numberline {5.7}\sphinxstyleliteralintitle {\sphinxupquote {d2l}} API Document}{261}{section.5.7}%
\contentsline {chapter}{Bibliography}{263}{chapter*.108}%
