
.. _sec_d2l:

``d2l`` API Document
====================


The implementations of the following members of the ``d2l`` package and
sections where they are defined and explained can be found in the
`source file <https://github.com/d2l-ai/d2l-en/tree/master/d2l>`__.



.. automodule:: d2l.mxnet
   :members:
   :imported-members:
