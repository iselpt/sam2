
.. _sec_linear_concise:

Concise Implementation of Linear Regression
===========================================


Broad and intense interest in deep learning for the past several years
has inspired companies, academics, and hobbyists to develop a variety of
mature open source frameworks for automating the repetitive work of
implementing gradient-based learning algorithms. In
:numref:`sec_linear_scratch`, we relied only on (i) tensors for data
storage and linear algebra; and (ii) auto differentiation for
calculating gradients. In practice, because data iterators, loss
functions, optimizers, and neural network layers are so common, modern
libraries implement these components for us as well.

In this section, we will show you how to implement the linear regression
model from :numref:`sec_linear_scratch` concisely by using high-level
APIs of deep learning frameworks.

Generating the Dataset
----------------------

To start, we will generate the same dataset as in
:numref:`sec_linear_scratch`.

.. code:: python

    from d2l import mxnet as d2l
    from mxnet import autograd, gluon, np, npx
    npx.set_np()

.. code:: python

    true_w = np.array([2, -3.4])
    true_b = 4.2
    features, labels = d2l.synthetic_data(true_w, true_b, 1000)

Reading the Dataset
-------------------

Rather than rolling our own iterator, we can call upon the existing API
in a framework to read data. We pass in ``features`` and ``labels`` as
arguments and specify ``batch_size`` when instantiating a data iterator
object. Besides, the boolean value ``is_train`` indicates whether or not
we want the data iterator object to shuffle the data on each epoch (pass
through the dataset).

.. code:: python

    def load_array(data_arrays, batch_size, is_train=True):  #@save
        """Construct a Gluon data iterator."""
        dataset = gluon.data.ArrayDataset(*data_arrays)
        return gluon.data.DataLoader(dataset, batch_size, shuffle=is_train)

.. code:: python

    batch_size = 10
    data_iter = load_array((features, labels), batch_size)

Now we can use ``data_iter`` in much the same way as we called the
``data_iter`` function in :numref:`sec_linear_scratch`. To verify that
it is working, we can read and print the first minibatch of examples.
Comparing with :numref:`sec_linear_scratch`, here we use ``iter`` to
construct a Python iterator and use ``next`` to obtain the first item
from the iterator.

.. code:: python

    next(iter(data_iter))

Defining the Model
------------------

When we implemented linear regression from scratch in
:numref:`sec_linear_scratch`, we defined our model parameters
explicitly and coded up the calculations to produce output using basic
linear algebra operations. You *should* know how to do this. But once
your models get more complex, and once you have to do this nearly every
day, you will be glad for the assistance. The situation is similar to
coding up your own blog from scratch. Doing it once or twice is
rewarding and instructive, but you would be a lousy web developer if
every time you needed a blog you spent a month reinventing the wheel.

For standard operations, we can use a framework’s predefined layers,
which allow us to focus especially on the layers used to construct the
model rather than having to focus on the implementation. We will first
define a model variable ``net``, which will refer to an instance of the
``Sequential`` class. The ``Sequential`` class defines a container for
several layers that will be chained together. Given input data, a
``Sequential`` instance passes it through the first layer, in turn
passing the output as the second layer’s input and so forth. In the
following example, our model consists of only one layer, so we do not
really need ``Sequential``. But since nearly all of our future models
will involve multiple layers, we will use it anyway just to familiarize
you with the most standard workflow.

Recall the architecture of a single-layer network as shown in
:numref:`fig_single_neuron`. The layer is said to be *fully-connected*
because each of its inputs is connected to each of its outputs by means
of a matrix-vector multiplication.

In Gluon, the fully-connected layer is defined in the ``Dense`` class.
Since we only want to generate a single scalar output, we set that
number to 1.

It is worth noting that, for convenience, Gluon does not require us to
specify the input shape for each layer. So here, we do not need to tell
Gluon how many inputs go into this linear layer. When we first try to
pass data through our model, e.g., when we execute ``net(X)`` later,
Gluon will automatically infer the number of inputs to each layer. We
will describe how this works in more detail later.

.. code:: python

    # `nn` is an abbreviation for neural networks
    from mxnet.gluon import nn
    net = nn.Sequential()
    net.add(nn.Dense(1))

Initializing Model Parameters
-----------------------------

Before using ``net``, we need to initialize the model parameters, such
as the weights and bias in the linear regression model. Deep learning
frameworks often have a predefined way to initialize the parameters.
Here we specify that each weight parameter should be randomly sampled
from a normal distribution with mean 0 and standard deviation 0.01. The
bias parameter will be initialized to zero.

We will import the ``initializer`` module from MXNet. This module
provides various methods for model parameter initialization. Gluon makes
``init`` available as a shortcut (abbreviation) to access the
``initializer`` package. We only specify how to initialize the weight by
calling ``init.Normal(sigma=0.01)``. Bias parameters are initialized to
zero by default.

.. code:: python

    from mxnet import init
    net.initialize(init.Normal(sigma=0.01))

The code above may look straightforward but you should note that
something strange is happening here. We are initializing parameters for
a network even though Gluon does not yet know how many dimensions the
input will have! It might be 2 as in our example or it might be 2000.
Gluon lets us get away with this because behind the scene, the
initialization is actually *deferred*. The real initialization will take
place only when we for the first time attempt to pass data through the
network. Just be careful to remember that since the parameters have not
been initialized yet, we cannot access or manipulate them.

Defining the Loss Function
--------------------------

In Gluon, the ``loss`` module defines various loss functions. In this
example, we will use the Gluon implementation of squared loss
(``L2Loss``).

.. code:: python

    loss = gluon.loss.L2Loss()

Defining the Optimization Algorithm
-----------------------------------

Minibatch stochastic gradient descent is a standard tool for optimizing
neural networks and thus Gluon supports it alongside a number of
variations on this algorithm through its ``Trainer`` class. When we
instantiate ``Trainer``, we will specify the parameters to optimize over
(obtainable from our model ``net`` via ``net.collect_params()``), the
optimization algorithm we wish to use (``sgd``), and a dictionary of
hyperparameters required by our optimization algorithm. Minibatch
stochastic gradient descent just requires that we set the value
``learning_rate``, which is set to 0.03 here.

.. code:: python

    from mxnet import gluon
    trainer = gluon.Trainer(net.collect_params(), 'sgd', {'learning_rate': 0.03})

Training
--------

You might have noticed that expressing our model through high-level APIs
of a deep learning framework requires comparatively few lines of code.
We did not have to individually allocate parameters, define our loss
function, or implement minibatch stochastic gradient descent. Once we
start working with much more complex models, advantages of high-level
APIs will grow considerably. However, once we have all the basic pieces
in place, the training loop itself is strikingly similar to what we did
when implementing everything from scratch.

To refresh your memory: for some number of epochs, we will make a
complete pass over the dataset (``train_data``), iteratively grabbing
one minibatch of inputs and the corresponding ground-truth labels. For
each minibatch, we go through the following ritual:

-  Generate predictions by calling ``net(X)`` and calculate the loss
   ``l`` (the forward propagation).
-  Calculate gradients by running the backpropagation.
-  Update the model parameters by invoking our optimizer.

For good measure, we compute the loss after each epoch and print it to
monitor progress.

.. code:: python

    num_epochs = 3
    for epoch in range(num_epochs):
        for X, y in data_iter:
            with autograd.record():
                l = loss(net(X), y)
            l.backward()
            trainer.step(batch_size)
        l = loss(net(features), labels)
        print(f'epoch {epoch + 1}, loss {l.mean().asnumpy():f}')

Below, we compare the model parameters learned by training on finite
data and the actual parameters that generated our dataset. To access
parameters, we first access the layer that we need from ``net`` and then
access that layer’s weights and bias. As in our from-scratch
implementation, note that our estimated parameters are close to their
ground-truth counterparts.

.. code:: python

    w = net[0].weight.data()
    print(f'error in estimating w: {true_w - w.reshape(true_w.shape)}')
    b = net[0].bias.data()
    print(f'error in estimating b: {true_b - b}')

Summary
-------

-  Using Gluon, we can implement models much more concisely.
-  In Gluon, the ``data`` module provides tools for data processing, the
   ``nn`` module defines a large number of neural network layers, and
   the ``loss`` module defines many common loss functions.
-  MXNet’s module ``initializer`` provides various methods for model
   parameter initialization.
-  Dimensionality and storage are automatically inferred, but be careful
   not to attempt to access parameters before they have been
   initialized.

Exercises
---------

1. If we replace ``l = loss(output, y)`` with
   ``l = loss(output, y).mean()``, we need to change
   ``trainer.step(batch_size)`` to ``trainer.step(1)`` for the code to
   behave identically. Why?
2. Review the MXNet documentation to see what loss functions and
   initialization methods are provided in the modules ``gluon.loss`` and
   ``init``. Replace the loss by Huber’s loss.
3. How do you access the gradient of ``dense.weight``?

`Discussions <https://discuss.d2l.ai/t/44>`__
