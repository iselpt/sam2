
Dive into Deep Learning
=======================



.. raw:: html
   :file: frontpage.html

.. toctree::
   :maxdepth: 1

   chapter_preface/index
   chapter_installation/index
   chapter_notation/index

.. toctree::
   :maxdepth: 2
   :numbered:

   chapter_introduction/index
   chapter_preliminaries/index
   chapter_linear-networks/index
   chapter_appendix-mathematics-for-deep-learning/index
   chapter_appendix-tools-for-deep-learning/index

.. toctree::
   :maxdepth: 1

   chapter_references/zreferences
