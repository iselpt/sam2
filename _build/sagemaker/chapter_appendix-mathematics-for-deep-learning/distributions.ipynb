{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Installing (updating) the following libraries for your Sagemaker\n",
    "instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install ..  # installing d2l\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 0
   },
   "source": [
    "# Distributions\n",
    ":label:`sec_distributions`\n",
    "\n",
    "Now that we have learned how to work with probability in both the discrete and the continuous setting, let us get to know some of the common distributions encountered.  Depending on the area of machine learning, we may need to be familiar with vastly more of these, or for some areas of deep learning potentially none at all.  This is, however, a good basic list to be familiar with.  Let us first import some common libraries.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 1,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from d2l import mxnet as d2l\n",
    "from IPython import display\n",
    "from math import erf, factorial\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 4
   },
   "source": [
    "## Bernoulli\n",
    "\n",
    "This is the simplest random variable usually encountered.  This random variable encodes a coin flip which comes up $1$ with probability $p$ and $0$ with probability $1-p$.  If we have a random variable $X$ with this distribution, we will write\n",
    "\n",
    "$$\n",
    "X \\sim \\mathrm{Bernoulli}(p).\n",
    "$$\n",
    "\n",
    "The cumulative distribution function is \n",
    "\n",
    "$$F(x) = \\begin{cases} 0 & x < 0, \\\\ 1-p & 0 \\le x < 1, \\\\ 1 & x >= 1 . \\end{cases}$$\n",
    ":eqlabel:`eq_bernoulli_cdf`\n",
    "\n",
    "The probability mass function is plotted below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 5,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "p = 0.3\n",
    "\n",
    "d2l.set_figsize()\n",
    "d2l.plt.stem([0, 1], [1 - p, p], use_line_collection=True)\n",
    "d2l.plt.xlabel('x')\n",
    "d2l.plt.ylabel('p.m.f.')\n",
    "d2l.plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 6
   },
   "source": [
    "Now, let us plot the cumulative distribution function :eqref:`eq_bernoulli_cdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 7,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "x = np.arange(-1, 2, 0.01)\n",
    "\n",
    "def F(x):\n",
    "    return 0 if x < 0 else 1 if x > 1 else 1 - p\n",
    "\n",
    "d2l.plot(x, np.array([F(y) for y in x]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 10
   },
   "source": [
    "If $X \\sim \\mathrm{Bernoulli}(p)$, then:\n",
    "\n",
    "* $\\mu_X = p$,\n",
    "* $\\sigma_X^2 = p(1-p)$.\n",
    "\n",
    "We can sample an array of arbitrary shape from a Bernoulli random variable as follows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 11,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "1*(np.random.rand(10, 10) < p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 14
   },
   "source": [
    "## Discrete Uniform\n",
    "\n",
    "The next commonly encountered random variable is a discrete uniform.  For our discussion here, we will assume that it is supported on the integers $\\{1, 2, \\ldots, n\\}$, however any other set of values can be freely chosen.  The meaning of the word *uniform* in this context is that every possible value is equally likely.  The probability for each value $i \\in \\{1, 2, 3, \\ldots, n\\}$ is $p_i = \\frac{1}{n}$.  We will denote a random variable $X$ with this distribution as\n",
    "\n",
    "$$\n",
    "X \\sim U(n).\n",
    "$$\n",
    "\n",
    "The cumulative distribution function is \n",
    "\n",
    "$$F(x) = \\begin{cases} 0 & x < 1, \\\\ \\frac{k}{n} & k \\le x < k+1 \\text{ with } 1 \\le k < n, \\\\ 1 & x >= n . \\end{cases}$$\n",
    ":eqlabel:`eq_discrete_uniform_cdf`\n",
    "\n",
    "Let us first plot the probability mass function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 15,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "n = 5\n",
    "\n",
    "d2l.plt.stem([i+1 for i in range(n)], n*[1 / n], use_line_collection=True)\n",
    "d2l.plt.xlabel('x')\n",
    "d2l.plt.ylabel('p.m.f.')\n",
    "d2l.plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 16
   },
   "source": [
    "Now, let us plot the cumulative distribution function :eqref:`eq_discrete_uniform_cdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 17,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "x = np.arange(-1, 6, 0.01)\n",
    "\n",
    "def F(x):\n",
    "    return 0 if x < 1 else 1 if x > n else np.floor(x) / n\n",
    "\n",
    "d2l.plot(x, np.array([F(y) for y in x]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 20
   },
   "source": [
    "If $X \\sim U(n)$, then:\n",
    "\n",
    "* $\\mu_X = \\frac{1+n}{2}$,\n",
    "* $\\sigma_X^2 = \\frac{n^2-1}{12}$.\n",
    "\n",
    "We can sample an array of arbitrary shape from a discrete uniform random variable as follows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 21,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "np.random.randint(1, n, size=(10, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 24
   },
   "source": [
    "## Continuous Uniform\n",
    "\n",
    "Next, let us discuss the continuous uniform distribution. The idea behind this random variable is that if we increase the $n$ in the discrete uniform distribution, and then scale it to fit within the interval $[a, b]$, we will approach a continuous random variable that just picks an arbitrary value in $[a, b]$ all with equal probability.  We will denote this distribution as\n",
    "\n",
    "$$\n",
    "X \\sim U(a, b).\n",
    "$$\n",
    "\n",
    "The probability density function is \n",
    "\n",
    "$$p(x) = \\begin{cases} \\frac{1}{b-a} & x \\in [a, b], \\\\ 0 & x \\not\\in [a, b].\\end{cases}$$\n",
    ":eqlabel:`eq_cont_uniform_pdf`\n",
    "\n",
    "The cumulative distribution function is \n",
    "\n",
    "$$F(x) = \\begin{cases} 0 & x < a, \\\\ \\frac{x-a}{b-a} & x \\in [a, b], \\\\ 1 & x >= b . \\end{cases}$$\n",
    ":eqlabel:`eq_cont_uniform_cdf`\n",
    "\n",
    "Let us first plot the probability density function :eqref:`eq_cont_uniform_pdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 25,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "a, b = 1, 3\n",
    "\n",
    "x = np.arange(0, 4, 0.01)\n",
    "p = (x > a)*(x < b)/(b - a)\n",
    "\n",
    "d2l.plot(x, p, 'x', 'p.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 28
   },
   "source": [
    "Now, let us plot the cumulative distribution function :eqref:`eq_cont_uniform_cdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 29,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def F(x):\n",
    "    return 0 if x < a else 1 if x > b else (x - a) / (b - a)\n",
    "\n",
    "d2l.plot(x, np.array([F(y) for y in x]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 32
   },
   "source": [
    "If $X \\sim U(a, b)$, then:\n",
    "\n",
    "* $\\mu_X = \\frac{a+b}{2}$,\n",
    "* $\\sigma_X^2 = \\frac{(b-a)^2}{12}$.\n",
    "\n",
    "We can sample an array of arbitrary shape from a uniform random variable as follows.  Note that it by default samples from a $U(0,1)$, so if we want a different range we need to scale it.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 33,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "(b - a) * np.random.rand(10, 10) + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 36
   },
   "source": [
    "## Binomial\n",
    "\n",
    "Let us make things a little more complex and examine the *binomial* random variable.  This random variable originates from performing a sequence of $n$ independent experiments, each of which has probability $p$ of succeeding, and asking how many successes we expect to see.\n",
    "\n",
    "Let us express this mathematically.  Each experiment is an independent random variable $X_i$ where we will use $1$ to encode success, and $0$ to encode failure.  Since each is an independent coin flip which is successful with probability $p$, we can say that $X_i \\sim \\mathrm{Bernoulli}(p)$.  Then, the binomial random variable is\n",
    "\n",
    "$$\n",
    "X = \\sum_{i=1}^n X_i.\n",
    "$$\n",
    "\n",
    "In this case, we will write\n",
    "\n",
    "$$\n",
    "X \\sim \\mathrm{Binomial}(n, p).\n",
    "$$\n",
    "\n",
    "To get the cumulative distribution function, we need to notice that getting exactly $k$ successes can occur in $\\binom{n}{k} = \\frac{n!}{k!(n-k)!}$ ways each of which has a probability of $p^k(1-p)^{n-k}$ of occurring.  Thus the cumulative distribution function is\n",
    "\n",
    "$$F(x) = \\begin{cases} 0 & x < 0, \\\\ \\sum_{m \\le k} \\binom{n}{m} p^m(1-p)^{n-m}  & k \\le x < k+1 \\text{ with } 0 \\le k < n, \\\\ 1 & x >= n . \\end{cases}$$\n",
    ":eqlabel:`eq_binomial_cdf`\n",
    "\n",
    "Let us first plot the probability mass function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 37,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "n, p = 10, 0.2\n",
    "\n",
    "# Compute binomial coefficient\n",
    "def binom(n, k):\n",
    "    comb = 1\n",
    "    for i in range(min(k, n - k)):\n",
    "        comb = comb * (n - i) // (i + 1)\n",
    "    return comb\n",
    "\n",
    "pmf = np.array([p**i * (1-p)**(n - i) * binom(n, i) for i in range(n + 1)])\n",
    "\n",
    "d2l.plt.stem([i for i in range(n + 1)], pmf, use_line_collection=True)\n",
    "d2l.plt.xlabel('x')\n",
    "d2l.plt.ylabel('p.m.f.')\n",
    "d2l.plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 40
   },
   "source": [
    "Now, let us plot the cumulative distribution function :eqref:`eq_binomial_cdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 41,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "x = np.arange(-1, 11, 0.01)\n",
    "cmf = np.cumsum(pmf)\n",
    "\n",
    "def F(x):\n",
    "    return 0 if x < 0 else 1 if x > n else cmf[int(x)]\n",
    "\n",
    "d2l.plot(x, np.array([F(y) for y in x.tolist()]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 44
   },
   "source": [
    "While this result is not simple, the means and variances are.  If $X \\sim \\mathrm{Binomial}(n, p)$, then:\n",
    "\n",
    "* $\\mu_X = np$,\n",
    "* $\\sigma_X^2 = np(1-p)$.\n",
    "\n",
    "This can be sampled as follows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 45,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "np.random.binomial(n, p, size=(10, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 48
   },
   "source": [
    "## Poisson\n",
    "Let us now perform a thought experiment.  We are standing at a bus stop and we want to know how many buses will arrive in the next minute.  Let us start by considering $X^{(1)} \\sim \\mathrm{Bernoulli}(p)$ which is simply the probability that a bus arrives in the one minute window.  For bus stops far from an urban center, this might be a pretty good approximation.  We may never see more than one bus in a minute.\n",
    "\n",
    "However, if we are in a busy area, it is possible or even likely that two buses will arrive.  We can model this by splitting our random variable into two parts for the first 30 seconds, or the second 30 seconds.  In this case we can write\n",
    "\n",
    "$$\n",
    "X^{(2)} \\sim X^{(2)}_1 + X^{(2)}_2,\n",
    "$$\n",
    "\n",
    "where $X^{(2)}$ is the total sum, and $X^{(2)}_i \\sim \\mathrm{Bernoulli}(p/2)$.  The total distribution is then $X^{(2)} \\sim \\mathrm{Binomial}(2, p/2)$.\n",
    "\n",
    "Why stop here?  Let us continue to split that minute into $n$ parts.  By the same reasoning as above, we see that\n",
    "\n",
    "$$X^{(n)} \\sim \\mathrm{Binomial}(n, p/n).$$\n",
    ":eqlabel:`eq_eq_poisson_approx`\n",
    "\n",
    "Consider these random variables.  By the previous section, we know that :eqref:`eq_eq_poisson_approx` has mean $\\mu_{X^{(n)}} = n(p/n) = p$, and variance $\\sigma_{X^{(n)}}^2 = n(p/n)(1-(p/n)) = p(1-p/n)$.  If we take $n \\rightarrow \\infty$, we can see that these numbers stabilize to $\\mu_{X^{(\\infty)}} = p$, and variance $\\sigma_{X^{(\\infty)}}^2 = p$.  This indicates that there *could be* some random variable we can define in this infinite subdivision limit.  \n",
    "\n",
    "This should not come as too much of a surprise, since in the real world we can just count the number of bus arrivals, however it is nice to see that our mathematical model is well defined.  This discussion can be made formal as the *law of rare events*.\n",
    "\n",
    "Following through this reasoning carefully, we can arrive at the following model.  We will say that $X \\sim \\mathrm{Poisson}(\\lambda)$ if it is a random variable which takes the values $\\{0,1,2, \\ldots\\}$ with probability\n",
    "\n",
    "$$p_k = \\frac{\\lambda^ke^{-\\lambda}}{k!}.$$\n",
    ":eqlabel:`eq_poisson_mass`\n",
    "\n",
    "The value $\\lambda > 0$ is known as the *rate* (or the *shape* parameter), and denotes the average number of arrivals we expect in one unit of time.  \n",
    "\n",
    "We may sum this probability mass function to get the cumulative distribution function.\n",
    "\n",
    "$$F(x) = \\begin{cases} 0 & x < 0, \\\\ e^{-\\lambda}\\sum_{m = 0}^k \\frac{\\lambda^m}{m!} & k \\le x < k+1 \\text{ with } 0 \\le k. \\end{cases}$$\n",
    ":eqlabel:`eq_poisson_cdf`\n",
    "\n",
    "Let us first plot the probability mass function :eqref:`eq_poisson_mass`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 49,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "lam = 5.0\n",
    "\n",
    "xs = [i for i in range(20)]\n",
    "pmf = np.array([np.exp(-lam) * lam**k / factorial(k) for k in xs])\n",
    "\n",
    "d2l.plt.stem(xs, pmf, use_line_collection=True)\n",
    "d2l.plt.xlabel('x')\n",
    "d2l.plt.ylabel('p.m.f.')\n",
    "d2l.plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 52
   },
   "source": [
    "Now, let us plot the cumulative distribution function :eqref:`eq_poisson_cdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 53,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "x = np.arange(-1, 21, 0.01)\n",
    "cmf = np.cumsum(pmf)\n",
    "def F(x):\n",
    "    return 0 if x < 0 else 1 if x > n else cmf[int(x)]\n",
    "\n",
    "d2l.plot(x, np.array([F(y) for y in x.tolist()]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 56
   },
   "source": [
    "As we saw above, the means and variances are particularly concise.  If $X \\sim \\mathrm{Poisson}(\\lambda)$, then:\n",
    "\n",
    "* $\\mu_X = \\lambda$,\n",
    "* $\\sigma_X^2 = \\lambda$.\n",
    "\n",
    "This can be sampled as follows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 57,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "np.random.poisson(lam, size=(10, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 60
   },
   "source": [
    "## Gaussian\n",
    "Now Let us try a different, but related experiment.  Let us say we again are performing $n$ independent $\\mathrm{Bernoulli}(p)$ measurements $X_i$.  The distribution of the sum of these is $X^{(n)} \\sim \\mathrm{Binomial}(n, p)$.  Rather than taking a limit as $n$ increases and $p$ decreases, Let us fix $p$, and then send $n \\rightarrow \\infty$.  In this case $\\mu_{X^{(n)}} = np \\rightarrow \\infty$ and $\\sigma_{X^{(n)}}^2 = np(1-p) \\rightarrow \\infty$, so there is no reason to think this limit should be well defined.\n",
    "\n",
    "However, not all hope is lost!  Let us just make the mean and variance be well behaved by defining\n",
    "\n",
    "$$\n",
    "Y^{(n)} = \\frac{X^{(n)} - \\mu_{X^{(n)}}}{\\sigma_{X^{(n)}}}.\n",
    "$$\n",
    "\n",
    "This can be seen to have mean zero and variance one, and so it is plausible to believe that it will converge to some limiting distribution.  If we plot what these distributions look like, we will become even more convinced that it will work.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 61,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "p = 0.2\n",
    "ns = [1, 10, 100, 1000]\n",
    "d2l.plt.figure(figsize=(10, 3))\n",
    "for i in range(4):\n",
    "    n = ns[i]\n",
    "    pmf = np.array([p**i * (1-p)**(n-i) * binom(n, i) for i in range(n + 1)])\n",
    "    d2l.plt.subplot(1, 4, i + 1)\n",
    "    d2l.plt.stem([(i - n*p)/np.sqrt(n*p*(1 - p)) for i in range(n + 1)], pmf,\n",
    "                 use_line_collection=True)\n",
    "    d2l.plt.xlim([-4, 4])\n",
    "    d2l.plt.xlabel('x')\n",
    "    d2l.plt.ylabel('p.m.f.')\n",
    "    d2l.plt.title(\"n = {}\".format(n))\n",
    "d2l.plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 64
   },
   "source": [
    "One thing to note: compared to the Poisson case, we are now dividing by the standard deviation which means that we are squeezing the possible outcomes into smaller and smaller areas.  This is an indication that our limit will no longer be discrete, but rather a continuous.\n",
    "\n",
    "A derivation of what occurs is beyond the scope of this document, but the *central limit theorem* states that as $n \\rightarrow \\infty$, this will yield the Gaussian Distribution (or sometimes normal distribution).  More explicitly, for any $a, b$:\n",
    "\n",
    "$$\n",
    "\\lim_{n \\rightarrow \\infty} P(Y^{(n)} \\in [a, b]) = P(\\mathcal{N}(0,1) \\in [a, b]),\n",
    "$$\n",
    "\n",
    "where we say a random variable is normally distributed with given mean $\\mu$ and variance $\\sigma^2$, written $X \\sim \\mathcal{N}(\\mu, \\sigma^2)$ if $X$ has density\n",
    "\n",
    "$$p_X(x) = \\frac{1}{\\sqrt{2\\pi\\sigma^2}}e^{-\\frac{(x-\\mu)^2}{2\\sigma^2}}.$$\n",
    ":eqlabel:`eq_gaussian_pdf`\n",
    "\n",
    "Let us first plot the probability density function :eqref:`eq_gaussian_pdf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 65,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "mu, sigma = 0, 1\n",
    "\n",
    "x = np.arange(-3, 3, 0.01)\n",
    "p = 1 / np.sqrt(2 * np.pi * sigma**2) * np.exp(-(x - mu)**2 / (2 * sigma**2))\n",
    "\n",
    "d2l.plot(x, p, 'x', 'p.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 68
   },
   "source": [
    "Now, let us plot the cumulative distribution function.  It is beyond the scope of this appendix, but the Gaussian c.d.f. does not have a closed-form formula in terms of more elementary functions.  We will use `erf` which provides a way to compute this integral numerically.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 69,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def phi(x):\n",
    "    return (1.0 + erf((x - mu) / (sigma * np.sqrt(2)))) / 2.0\n",
    "\n",
    "d2l.plot(x, np.array([phi(y) for y in x.tolist()]), 'x', 'c.d.f.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 72
   },
   "source": [
    "Keen-eyed readers will recognize some of these terms.  Indeed, we encountered this integral in :numref:`sec_integral_calculus`.  Indeed we need exactly that computation to see that this $p_X(x)$ has total area one and is thus a valid density.\n",
    "\n",
    "Our choice of working with coin flips made computations shorter, but nothing about that choice was fundamental.  Indeed, if we take any collection of independent identically distributed random variables $X_i$, and form\n",
    "\n",
    "$$\n",
    "X^{(N)} = \\sum_{i=1}^N X_i.\n",
    "$$\n",
    "\n",
    "Then\n",
    "\n",
    "$$\n",
    "\\frac{X^{(N)} - \\mu_{X^{(N)}}}{\\sigma_{X^{(N)}}}\n",
    "$$\n",
    "\n",
    "will be approximately Gaussian.  There are additional requirements needed to make it work, most commonly $E[X^4] < \\infty$, but the philosophy is clear.\n",
    "\n",
    "The central limit theorem is the reason that the Gaussian is fundamental to probability, statistics, and machine learning.  Whenever we can say that something we measured is a sum of many small independent contributions, we can assume that the thing being measured will be close to Gaussian.  \n",
    "\n",
    "There are many more fascinating properties of Gaussians, and we would like to discuss one more here.  The Gaussian is what is known as a *maximum entropy distribution*.  We will get into entropy more deeply in :numref:`sec_information_theory`, however all we need to know at this point is that it is a measure of randomness.  In a rigorous mathematical sense, we can think of the Gaussian as the *most* random choice of random variable with fixed mean and variance.  Thus, if we know that our random variable has some mean and variance, the Gaussian is in a sense the most conservative choice of distribution we can make.\n",
    "\n",
    "To close the section, Let us recall that if $X \\sim \\mathcal{N}(\\mu, \\sigma^2)$, then:\n",
    "\n",
    "* $\\mu_X = \\mu$,\n",
    "* $\\sigma_X^2 = \\sigma^2$.\n",
    "\n",
    "We can sample from the Gaussian (or standard normal) distribution as shown below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 73,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "np.random.normal(mu, sigma, size=(10, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 76
   },
   "source": [
    "## Exponential Family\n",
    ":label:`subsec_exponential_family`\n",
    "\n",
    "One shared property for all the distributions listed above is that they all \n",
    "belong to which is known as the *exponential family*. The exponential family \n",
    "is a set of distributions whose density can be expressed in the following \n",
    "form:\n",
    "\n",
    "$$p(\\mathbf{x} | \\mathbf{\\eta}) = h(\\mathbf{x}) \\cdot \\mathrm{exp} \\big{(} \\eta^{\\top} \\cdot T\\mathbf(x) - A(\\mathbf{\\eta}) \\big{)}$$\n",
    ":eqlabel:`eq_exp_pdf`\n",
    "\n",
    "As this definition can be a little subtle, let us examine it closely.  \n",
    "\n",
    "First, $h(\\mathbf{x})$ is known as the *underlying measure* or the \n",
    "*base measure*.  This can be viewed as an original choice of measure we are \n",
    "modifying with our exponential weight.  \n",
    "\n",
    "Second, we have the vector $\\mathbf{\\eta} = (\\eta_1, \\eta_2, ..., \\eta_l) \\in \n",
    "\\mathbb{R}^l$ called the *natural parameters* or *canonical parameters*.  These\n",
    "define how the base measure will be modified.  The natural parameters enter \n",
    "into the new measure by taking the dot product of these parameters against \n",
    "some function $T(\\cdot)$ of $\\mathbf{x}= (x_1, x_2, ..., x_n) \\in \n",
    "\\mathbb{R}^n$ and exponentiated. $T(\\mathbf{x})= (T_1(\\mathbf{x}), \n",
    "T_2(\\mathbf{x}), ..., T_l(\\mathbf{x}))$ \n",
    "is called the *sufficient statistics* for $\\eta$. This name is used since the \n",
    "information represented by $T(\\mathbf{x})$ is sufficient to calculate the \n",
    "probability density and no other information from the sample $\\mathbf{x}$'s \n",
    "are required.\n",
    "\n",
    "Third, we have $A(\\mathbf{\\eta})$, which is referred to as the *cumulant \n",
    "function*, which ensures that the above distribution :eqref:`eq_exp_pdf` \n",
    "integrates to one, i.e.,\n",
    "\n",
    "$$  A(\\mathbf{\\eta}) = \\log \\left[\\int h(\\mathbf{x}) \\cdot \\mathrm{exp} \n",
    "\\big{(}\\eta^{\\top} \\cdot T\\mathbf(x) \\big{)} dx \\right].$$\n",
    "\n",
    "To be concrete, let us consider the Gaussian. Assuming that $\\mathbf{x}$ is \n",
    "an univariate variable, we saw that it had a density of\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "p(x | \\mu, \\sigma) &= \\frac{1}{\\sqrt{2 \\pi \\sigma^2}} \\mathrm{exp} \n",
    "\\Big{\\{} \\frac{-(x-\\mu)^2}{2 \\sigma^2} \\Big{\\}} \\\\\n",
    "&= \\frac{1}{\\sqrt{2 \\pi}} \\cdot \\mathrm{exp} \\Big{\\{} \\frac{\\mu}{\\sigma^2}x \n",
    "- \\frac{1}{2 \\sigma^2} x^2 - \\big{(} \\frac{1}{2 \\sigma^2} \\mu^2 \n",
    "+ \\log(\\sigma) \\big{)} \\Big{\\}} .\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "This matches the definition of the exponential family with:\n",
    "\n",
    "* *underlying measure*: $h(x) = \\frac{1}{\\sqrt{2 \\pi}}$,\n",
    "* *natural parameters*: $\\eta = \\begin{bmatrix} \\eta_1 \\\\ \\eta_2 \n",
    "\\end{bmatrix} = \\begin{bmatrix} \\frac{\\mu}{\\sigma^2} \\\\ \n",
    "\\frac{1}{2 \\sigma^2}  \\end{bmatrix}$,\n",
    "* *sufficient statistics*: $T(x) = \\begin{bmatrix}x\\\\-x^2\\end{bmatrix}$, and\n",
    "* *cumulant function*: $A(\\eta) = \\frac{1}{2 \\sigma^2} \\mu^2 + \\log(\\sigma)  \n",
    "= \\frac{\\eta_1^2}{4 \\eta_2} - \\frac{1}{2}\\log(2 \\eta_2)$.\n",
    "\n",
    "It is worth noting that the exact choice of each of above terms is somewhat \n",
    "arbitrary.  Indeed, the important feature is that the distribution can be \n",
    "expressed in this form, not the exact form itself.\n",
    "\n",
    "As we allude to in :numref:`subsec_softmax_and_derivatives`, a widely used \n",
    "technique is to assume that the  final output $\\mathbf{y}$ follows an \n",
    "exponential family distribution. The exponential family is a common and \n",
    "powerful family of distributions encountered frequently in machine learning.\n",
    "\n",
    "\n",
    "## Summary\n",
    "* Bernoulli random variables can be used to model events with a yes/no outcome.\n",
    "* Discrete uniform distributions model selects from a finite set of possibilities.\n",
    "* Continuous uniform distributions select from an interval.\n",
    "* Binomial distributions model a series of Bernoulli random variables, and count the number of successes.\n",
    "* Poisson random variables model the arrival of rare events.\n",
    "* Gaussian random variables model the result of adding a large number of independent random variables together.\n",
    "* All the above distributions belong to exponential family.\n",
    "\n",
    "## Exercises\n",
    "\n",
    "1. What is the standard deviation of a random variable that is the difference $X-Y$ of two independent binomial random variables $X, Y \\sim \\mathrm{Binomial}(16, 1/2)$.\n",
    "2. If we take a Poisson random variable $X \\sim \\mathrm{Poisson}(\\lambda)$ and consider $(X - \\lambda)/\\sqrt{\\lambda}$ as $\\lambda \\rightarrow \\infty$, we can show that this becomes approximately Gaussian.  Why does this make sense?\n",
    "3. What is the probability mass function for a sum of two discrete uniform random variables on $n$ elements?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 77,
    "tab": [
     "mxnet"
    ]
   },
   "source": [
    "[Discussions](https://discuss.d2l.ai/t/417)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_mxnet_p36",
   "name": "conda_mxnet_p36"
  },
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}