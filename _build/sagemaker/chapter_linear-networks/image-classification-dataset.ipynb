{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Installing (updating) the following libraries for your Sagemaker\n",
    "instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install -U mxnet-cu101mkl==1.6.0.post0  # updating mxnet to at least v1.6\n",
    "!pip install ..  # installing d2l\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 0
   },
   "source": [
    "# The Image Classification Dataset\n",
    ":label:`sec_fashion_mnist`\n",
    "\n",
    "One of the widely used dataset for image classification is the  MNIST dataset :cite:`LeCun.Bottou.Bengio.ea.1998`.\n",
    "While it had a good run as a benchmark dataset,\n",
    "even simple models by today's standards achieve classification accuracy over 95%,\n",
    "making it unsuitable for distinguishing between stronger models and weaker ones.\n",
    "Today, MNIST serves as more of sanity checks than as a benchmark.\n",
    "To up the ante just a bit, we will focus our discussion in the coming sections\n",
    "on the qualitatively similar, but comparatively complex Fashion-MNIST\n",
    "dataset :cite:`Xiao.Rasul.Vollgraf.2017`, which was released in 2017.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 1,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from d2l import mxnet as d2l\n",
    "from mxnet import gluon\n",
    "import sys\n",
    "\n",
    "d2l.use_svg_display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 4
   },
   "source": [
    "## Reading the Dataset\n",
    "\n",
    "We can download and read the Fashion-MNIST dataset into memory via the build-in functions in the framework.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 5,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "mnist_train = gluon.data.vision.FashionMNIST(train=True)\n",
    "mnist_test = gluon.data.vision.FashionMNIST(train=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 8
   },
   "source": [
    "Fashion-MNIST consists of images from 10 categories, each represented\n",
    "by 6000 images in the training dataset and by 1000 in the test dataset.\n",
    "A *test dataset* (or *test set*) is used for evaluating  model performance and not for training.\n",
    "Consequently the training set and the test set\n",
    "contain 60000 and 10000 images, respectively.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 9,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "len(mnist_train), len(mnist_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 11
   },
   "source": [
    "The height and width of each input image are both 28 pixels.\n",
    "Note that the dataset consists of grayscale images, whose number of channels is 1.\n",
    "For brevity, throughout this book\n",
    "we store the shape of any image with height $h$ width $w$ pixels as $h \\times w$ or ($h$, $w$).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 12,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "mnist_train[0][0].shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 13
   },
   "source": [
    "The images in Fashion-MNIST are associated with the following categories:\n",
    "t-shirt, trousers, pullover, dress, coat, sandal, shirt, sneaker, bag, and ankle boot.\n",
    "The following function converts between numeric label indices and their names in text.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 14,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def get_fashion_mnist_labels(labels):  #@save\n",
    "    \"\"\"Return text labels for the Fashion-MNIST dataset.\"\"\"\n",
    "    text_labels = ['t-shirt', 'trouser', 'pullover', 'dress', 'coat',\n",
    "                   'sandal', 'shirt', 'sneaker', 'bag', 'ankle boot']\n",
    "    return [text_labels[int(i)] for i in labels]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 15
   },
   "source": [
    "We can now create a function to visualize these examples.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 16,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def show_images(imgs, num_rows, num_cols, titles=None, scale=1.5):  #@save\n",
    "    \"\"\"Plot a list of images.\"\"\"\n",
    "    figsize = (num_cols * scale, num_rows * scale)\n",
    "    _, axes = d2l.plt.subplots(num_rows, num_cols, figsize=figsize)\n",
    "    axes = axes.flatten()\n",
    "    for i, (ax, img) in enumerate(zip(axes, imgs)):\n",
    "        ax.imshow(d2l.numpy(img))\n",
    "        ax.axes.get_xaxis().set_visible(False)\n",
    "        ax.axes.get_yaxis().set_visible(False)\n",
    "        if titles:\n",
    "            ax.set_title(titles[i])\n",
    "    return axes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 17
   },
   "source": [
    "Here are the images and their corresponding labels (in text)\n",
    "for the first few examples in the training dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 18,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "X, y = mnist_train[:18]\n",
    "show_images(X.squeeze(axis=-1), 2, 9, titles=get_fashion_mnist_labels(y));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 21
   },
   "source": [
    "## Reading a Minibatch\n",
    "\n",
    "To make our life easier when reading from the training and test sets,\n",
    "we use the built-in data iterator rather than creating one from scratch.\n",
    "Recall that at each iteration, a data loader\n",
    "reads a minibatch of data with size `batch_size` each time.\n",
    "We also randomly shuffle the examples for the training data iterator.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 22,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "batch_size = 256\n",
    "\n",
    "def get_dataloader_workers():  #@save\n",
    "    \"\"\"Use 4 processes to read the data except for Windows.\"\"\"\n",
    "    return 0 if sys.platform.startswith('win') else 4\n",
    "\n",
    "# `ToTensor` converts the image data from uint8 to 32-bit floating point. It\n",
    "# divides all numbers by 255 so that all pixel values are between 0 and 1\n",
    "transformer = gluon.data.vision.transforms.ToTensor()\n",
    "train_iter = gluon.data.DataLoader(mnist_train.transform_first(transformer),\n",
    "                                   batch_size, shuffle=True,\n",
    "                                   num_workers=get_dataloader_workers())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 25
   },
   "source": [
    "Let us look at the time it takes to read the training data.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 26,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "timer = d2l.Timer()\n",
    "for X, y in train_iter:\n",
    "    continue\n",
    "f'{timer.stop():.2f} sec'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 27
   },
   "source": [
    "## Putting All Things Together\n",
    "\n",
    "Now we define the `load_data_fashion_mnist` function\n",
    "that obtains and reads the Fashion-MNIST dataset.\n",
    "It returns the data iterators for both the training set and validation set.\n",
    "In addition, it accepts an optional argument to resize images to another shape.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 28,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def load_data_fashion_mnist(batch_size, resize=None):  #@save\n",
    "    \"\"\"Download the Fashion-MNIST dataset and then load it into memory.\"\"\"\n",
    "    dataset = gluon.data.vision\n",
    "    trans = [dataset.transforms.ToTensor()]\n",
    "    if resize:\n",
    "        trans.insert(0, dataset.transforms.Resize(resize))\n",
    "    trans = dataset.transforms.Compose(trans)\n",
    "    mnist_train = dataset.FashionMNIST(train=True).transform_first(trans)\n",
    "    mnist_test = dataset.FashionMNIST(train=False).transform_first(trans)\n",
    "    return (gluon.data.DataLoader(mnist_train, batch_size, shuffle=True,\n",
    "                                  num_workers=get_dataloader_workers()),\n",
    "            gluon.data.DataLoader(mnist_test, batch_size, shuffle=False,\n",
    "                                  num_workers=get_dataloader_workers()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 31
   },
   "source": [
    "Below we test the image resizing feature of the `load_data_fashion_mnist` function\n",
    "by specifying the `resize` argument.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 32,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "train_iter, test_iter = load_data_fashion_mnist(32, resize=64)\n",
    "for X, y in train_iter:\n",
    "    print(X.shape, X.dtype, y.shape, y.dtype)\n",
    "    break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 33
   },
   "source": [
    "We are now ready to work with the Fashion-MNIST dataset in the sections that follow.\n",
    "\n",
    "## Summary\n",
    "\n",
    "* Fashion-MNIST is an apparel classification dataset consisting of images representing 10 categories. We will use this dataset in subsequent sections and chapters to evaluate various classification algorithms.\n",
    "* We store the shape of any image with height $h$ width $w$ pixels as $h \\times w$ or ($h$, $w$).\n",
    "* Data iterators are a key component for efficient performance. Rely on well-implemented data iterators that exploit high-performance computing to avoid slowing down your training loop.\n",
    "\n",
    "\n",
    "## Exercises\n",
    "\n",
    "1. Does reducing the `batch_size` (for instance, to 1) affect the reading performance?\n",
    "1. The data iterator performance is important. Do you think the current implementation is fast enough? Explore various options to improve it.\n",
    "1. Check out the framework's online API documentation. Which other datasets are available?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 34,
    "tab": [
     "mxnet"
    ]
   },
   "source": [
    "[Discussions](https://discuss.d2l.ai/t/48)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_mxnet_p36",
   "name": "conda_mxnet_p36"
  },
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}