{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Installing (updating) the following libraries for your Sagemaker\n",
    "instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install -U mxnet-cu101mkl==1.6.0.post0  # updating mxnet to at least v1.6\n",
    "!pip install ..  # installing d2l\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 0
   },
   "source": [
    "# Linear Regression Implementation from Scratch\n",
    ":label:`sec_linear_scratch`\n",
    "\n",
    "Now that you understand the key ideas behind linear regression,\n",
    "we can begin to work through a hands-on implementation in code.\n",
    "In this section, we will implement the entire method from scratch,\n",
    "including the data pipeline, the model,\n",
    "the loss function, and the minibatch stochastic gradient descent optimizer.\n",
    "While modern deep learning frameworks can automate nearly all of this work,\n",
    "implementing things from scratch is the only way\n",
    "to make sure that you really know what you are doing.\n",
    "Moreover, when it comes time to customize models,\n",
    "defining our own layers or loss functions,\n",
    "understanding how things work under the hood will prove handy.\n",
    "In this section, we will rely only on tensors and auto differentiation.\n",
    "Afterwards, we will introduce a more concise implementation,\n",
    "taking advantage of bells and whistles of deep learning frameworks.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 1,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from d2l import mxnet as d2l\n",
    "from mxnet import autograd, np, npx\n",
    "import random\n",
    "npx.set_np()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 4
   },
   "source": [
    "## Generating the Dataset\n",
    "\n",
    "To keep things simple, we will construct an artificial dataset\n",
    "according to a linear model with additive noise.\n",
    "Our task will be to recover this model's parameters\n",
    "using the finite set of examples contained in our dataset.\n",
    "We will keep the data low-dimensional so we can visualize it easily.\n",
    "In the following code snippet, we generate a dataset\n",
    "containing 1000 examples, each consisting of 2 features\n",
    "sampled from a standard normal distribution.\n",
    "Thus our synthetic dataset will be a matrix\n",
    "$\\mathbf{X}\\in \\mathbb{R}^{1000 \\times 2}$.\n",
    "\n",
    "The true parameters generating our dataset will be\n",
    "$\\mathbf{w} = [2, -3.4]^\\top$ and $b = 4.2$,\n",
    "and our synthetic labels will be assigned according\n",
    "to the following linear model with the noise term $\\epsilon$:\n",
    "\n",
    "$$\\mathbf{y}= \\mathbf{X} \\mathbf{w} + b + \\mathbf\\epsilon.$$\n",
    "\n",
    "You could think of $\\epsilon$ as capturing potential\n",
    "measurement errors on the features and labels.\n",
    "We will assume that the standard assumptions hold and thus\n",
    "that $\\epsilon$ obeys a normal distribution with mean of 0.\n",
    "To make our problem easy, we will set its standard deviation to 0.01.\n",
    "The following code generates our synthetic dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 5,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def synthetic_data(w, b, num_examples):  #@save\n",
    "    \"\"\"Generate y = Xw + b + noise.\"\"\"\n",
    "    X = np.random.normal(0, 1, (num_examples, len(w)))\n",
    "    y = np.dot(X, w) + b\n",
    "    y += np.random.normal(0, 0.01, y.shape)\n",
    "    return X, y.reshape((-1, 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 7,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "true_w = np.array([2, -3.4])\n",
    "true_b = 4.2\n",
    "features, labels = synthetic_data(true_w, true_b, 1000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 8
   },
   "source": [
    "Note that each row in `features` consists of a 2-dimensional data example\n",
    "and that each row in `labels` consists of a 1-dimensional label value (a scalar).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 9,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "print('features:', features[0],'\\nlabel:', labels[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 10
   },
   "source": [
    "By generating a scatter plot using the second feature `features[:, 1]` and `labels`,\n",
    "we can clearly observe the linear correlation between the two.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 11,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "d2l.set_figsize()\n",
    "# The semicolon is for displaying the plot only\n",
    "d2l.plt.scatter(d2l.numpy(features[:, 1]), d2l.numpy(labels), 1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 12
   },
   "source": [
    "## Reading the Dataset\n",
    "\n",
    "Recall that training models consists of\n",
    "making multiple passes over the dataset,\n",
    "grabbing one minibatch of examples at a time,\n",
    "and using them to update our model.\n",
    "Since this process is so fundamental\n",
    "to training machine learning algorithms,\n",
    "it is worth defining a utility function\n",
    "to shuffle the dataset and access it in minibatches.\n",
    "\n",
    "In the following code, we define the `data_iter` function\n",
    "to demonstrate one possible implementation of this functionality.\n",
    "The function takes a batch size, a matrix of features,\n",
    "and a vector of labels, yielding minibatches of the size `batch_size`.\n",
    "Each minibatch consists of a tuple of features and labels.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 13,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def data_iter(batch_size, features, labels):\n",
    "    num_examples = len(features)\n",
    "    indices = list(range(num_examples))\n",
    "    # The examples are read at random, in no particular order\n",
    "    random.shuffle(indices)\n",
    "    for i in range(0, num_examples, batch_size):\n",
    "        batch_indices = np.array(\n",
    "            indices[i: min(i + batch_size, num_examples)])\n",
    "        yield features[batch_indices], labels[batch_indices]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 15
   },
   "source": [
    "In general, note that we want to use reasonably sized minibatches\n",
    "to take advantage of the GPU hardware,\n",
    "which excels at parallelizing operations.\n",
    "Because each example can be fed through our models in parallel\n",
    "and the gradient of the loss function for each example can also be taken in parallel,\n",
    "GPUs allow us to process hundreds of examples in scarcely more time\n",
    "than it might take to process just a single example.\n",
    "\n",
    "To build some intuition, let us read and print\n",
    "the first small batch of data examples.\n",
    "The shape of the features in each minibatch tells us\n",
    "both the minibatch size and the number of input features.\n",
    "Likewise, our minibatch of labels will have a shape given by `batch_size`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 16,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "batch_size = 10\n",
    "\n",
    "for X, y in data_iter(batch_size, features, labels):\n",
    "    print(X, '\\n', y)\n",
    "    break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 17
   },
   "source": [
    "As we run the iteration, we obtain distinct minibatches\n",
    "successively until the entire dataset has been exhausted (try this).\n",
    "While the iteration implemented above is good for didactic purposes,\n",
    "it is inefficient in ways that might get us in trouble on real problems.\n",
    "For example, it requires that we load all the data in memory\n",
    "and that we perform lots of random memory access.\n",
    "The built-in iterators implemented in a deep learning framework\n",
    "are considerably more efficient and they can deal\n",
    "with both data stored in files and data fed via data streams.\n",
    "\n",
    "\n",
    "## Initializing Model Parameters\n",
    "\n",
    "Before we can begin optimizing our model's parameters by minibatch stochastic gradient descent,\n",
    "we need to have some parameters in the first place.\n",
    "In the following code, we initialize weights by sampling\n",
    "random numbers from a normal distribution with mean 0\n",
    "and a standard deviation of 0.01, and setting the bias to 0.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 18,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "w = np.random.normal(0, 0.01, (2, 1))\n",
    "b = np.zeros(1)\n",
    "w.attach_grad()\n",
    "b.attach_grad()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 21
   },
   "source": [
    "After initializing our parameters,\n",
    "our next task is to update them until\n",
    "they fit our data sufficiently well.\n",
    "Each update requires taking the gradient\n",
    "of our loss function with respect to the parameters.\n",
    "Given this gradient, we can update each parameter\n",
    "in the direction that may reduce the loss.\n",
    "\n",
    "Since nobody wants to compute gradients explicitly\n",
    "(this is tedious and error prone),\n",
    "we use automatic differentiation,\n",
    "as introduced in :numref:`sec_autograd`, to compute the gradient.\n",
    "\n",
    "\n",
    "## Defining the Model\n",
    "\n",
    "Next, we must define our model,\n",
    "relating its inputs and parameters to its outputs.\n",
    "Recall that to calculate the output of the linear model,\n",
    "we simply take the matrix-vector dot product\n",
    "of the input features $\\mathbf{X}$ and the model weights $\\mathbf{w}$,\n",
    "and add the offset $b$ to each example.\n",
    "Note that below $\\mathbf{Xw}$  is a vector and $b$ is a scalar.\n",
    "Recall the broadcasting mechanism as described in :numref:`subsec_broadcasting`.\n",
    "When we add a vector and a scalar,\n",
    "the scalar is added to each component of the vector.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 22,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def linreg(X, w, b):  #@save\n",
    "    \"\"\"The linear regression model.\"\"\"\n",
    "    return np.dot(X, w) + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 23
   },
   "source": [
    "## Defining the Loss Function\n",
    "\n",
    "Since updating our model requires taking\n",
    "the gradient of our loss function,\n",
    "we ought to define the loss function first.\n",
    "Here we will use the squared loss function\n",
    "as described in :numref:`sec_linear_regression`.\n",
    "In the implementation, we need to transform the true value `y`\n",
    "into the predicted value's shape `y_hat`.\n",
    "The result returned by the following function\n",
    "will also have the same shape as `y_hat`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 24,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def squared_loss(y_hat, y):  #@save\n",
    "    \"\"\"Squared loss.\"\"\"\n",
    "    return (y_hat - y.reshape(y_hat.shape)) ** 2 / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 25
   },
   "source": [
    "## Defining the Optimization Algorithm\n",
    "\n",
    "As we discussed in :numref:`sec_linear_regression`,\n",
    "linear regression has a closed-form solution.\n",
    "However, this is not a book about linear regression:\n",
    "it is a book about deep learning.\n",
    "Since none of the other models that this book introduces\n",
    "can be solved analytically, we will take this opportunity to introduce your first working example of\n",
    "minibatch stochastic gradient descent.\n",
    "\n",
    "\n",
    "At each step, using one minibatch randomly drawn from our dataset,\n",
    "we will estimate the gradient of the loss with respect to our parameters.\n",
    "Next, we will update our parameters\n",
    "in the direction that may reduce the loss.\n",
    "The following code applies the minibatch stochastic gradient descent update,\n",
    "given a set of parameters, a learning rate, and a batch size.\n",
    "The size of the update step is determined by the learning rate `lr`.\n",
    "Because our loss is calculated as a sum over the minibatch of examples,\n",
    "we normalize our step size by the batch size (`batch_size`),\n",
    "so that the magnitude of a typical step size\n",
    "does not depend heavily on our choice of the batch size.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 26,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "def sgd(params, lr, batch_size):  #@save\n",
    "    \"\"\"Minibatch stochastic gradient descent.\"\"\"\n",
    "    for param in params:\n",
    "        param[:] = param - lr * param.grad / batch_size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 29
   },
   "source": [
    "## Training\n",
    "\n",
    "Now that we have all of the parts in place,\n",
    "we are ready to implement the main training loop.\n",
    "It is crucial that you understand this code\n",
    "because you will see nearly identical training loops\n",
    "over and over again throughout your career in deep learning.\n",
    "\n",
    "In each iteration, we will grab a minibatch of training examples,\n",
    "and pass them through our model to obtain a set of predictions.\n",
    "After calculating the loss, we initiate the backwards pass through the network,\n",
    "storing the gradients with respect to each parameter.\n",
    "Finally, we will call the optimization algorithm `sgd`\n",
    "to update the model parameters.\n",
    "\n",
    "In summary, we will execute the following loop:\n",
    "\n",
    "* Initialize parameters $(\\mathbf{w}, b)$\n",
    "* Repeat until done\n",
    "    * Compute gradient $\\mathbf{g} \\leftarrow \\partial_{(\\mathbf{w},b)} \\frac{1}{|\\mathcal{B}|} \\sum_{i \\in \\mathcal{B}} l(\\mathbf{x}^{(i)}, y^{(i)}, \\mathbf{w}, b)$\n",
    "    * Update parameters $(\\mathbf{w}, b) \\leftarrow (\\mathbf{w}, b) - \\eta \\mathbf{g}$\n",
    "\n",
    "In each *epoch*,\n",
    "we will iterate through the entire dataset\n",
    "(using the `data_iter` function) once\n",
    "passing through every example in the training dataset\n",
    "(assuming that the number of examples is divisible by the batch size).\n",
    "The number of epochs `num_epochs` and the learning rate `lr` are both hyperparameters,\n",
    "which we set here to 3 and 0.03, respectively.\n",
    "Unfortunately, setting hyperparameters is tricky\n",
    "and requires some adjustment by trial and error.\n",
    "We elide these details for now but revise them\n",
    "later in\n",
    ":numref:`chap_optimization`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 30,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "lr = 0.03\n",
    "num_epochs = 3\n",
    "net = linreg\n",
    "loss = squared_loss"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 31,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "for epoch in range(num_epochs):\n",
    "    for X, y in data_iter(batch_size, features, labels):\n",
    "        with autograd.record():\n",
    "            l = loss(net(X, w, b), y)  # Minibatch loss in `X` and `y`\n",
    "        # Because `l` has a shape (`batch_size`, 1) and is not a scalar\n",
    "        # variable, the elements in `l` are added together to obtain a new\n",
    "        # variable, on which gradients with respect to [`w`, `b`] are computed\n",
    "        l.backward()\n",
    "        sgd([w, b], lr, batch_size)  # Update parameters using their gradient\n",
    "    train_l = loss(net(features, w, b), labels)\n",
    "    print(f'epoch {epoch + 1}, loss {float(train_l.mean()):f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 34
   },
   "source": [
    "In this case, because we synthesized the dataset ourselves,\n",
    "we know precisely what the true parameters are.\n",
    "Thus, we can evaluate our success in training\n",
    "by comparing the true parameters\n",
    "with those that we learned through our training loop.\n",
    "Indeed they turn out to be very close to each other.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "origin_pos": 35,
    "tab": [
     "mxnet"
    ]
   },
   "outputs": [],
   "source": [
    "print(f'error in estimating w: {true_w - w.reshape(true_w.shape)}')\n",
    "print(f'error in estimating b: {true_b - b}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 36
   },
   "source": [
    "Note that we should not take it for granted\n",
    "that we are able to recover the parameters perfectly.\n",
    "However, in machine learning, we are typically less concerned\n",
    "with recovering true underlying parameters,\n",
    "and more concerned with parameters that lead to highly accurate prediction.\n",
    "Fortunately, even on difficult optimization problems,\n",
    "stochastic gradient descent can often find remarkably good solutions,\n",
    "owing partly to the fact that, for deep networks,\n",
    "there exist many configurations of the parameters\n",
    "that lead to highly accurate prediction.\n",
    "\n",
    "\n",
    "## Summary\n",
    "\n",
    "* We saw how a deep network can be implemented and optimized from scratch, using just tensors and auto differentiation, without any need for defining layers or fancy optimizers.\n",
    "* This section only scratches the surface of what is possible. In the following sections, we will describe additional models based on the concepts that we have just introduced and learn how to implement them more concisely.\n",
    "\n",
    "\n",
    "## Exercises\n",
    "\n",
    "1. What would happen if we were to initialize the weights to zero. Would the algorithm still work?\n",
    "1. Assume that you are\n",
    "   [Georg Simon Ohm](https://en.wikipedia.org/wiki/Georg_Ohm) trying to come up\n",
    "   with a model between voltage and current. Can you use auto differentiation to learn the parameters of your model?\n",
    "1. Can you use [Planck's Law](https://en.wikipedia.org/wiki/Planck%27s_law) to determine the temperature of an object using spectral energy density?\n",
    "1. What are the problems you might encounter if you wanted to  compute the second derivatives? How would you fix them?\n",
    "1.  Why is the `reshape` function needed in the `squared_loss` function?\n",
    "1. Experiment using different learning rates to find out how fast the loss function value drops.\n",
    "1. If the number of examples cannot be divided by the batch size, what happens to the `data_iter` function's behavior?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "origin_pos": 37,
    "tab": [
     "mxnet"
    ]
   },
   "source": [
    "[Discussions](https://discuss.d2l.ai/t/42)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "conda_mxnet_p36",
   "name": "conda_mxnet_p36"
  },
  "language_info": {
   "name": "python"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}